case 1 - m1 entirely before m2
[m1] [m2]

case 2 - m1 handoff to m2
[m1 ]
    [ m2]

case 3 - m1 then overlap then m2
[m1 ]
   [ m2]

case 4 - same start, m1 leaves
[m1 ]
[m2   ]

case 5 - m2 contains m1
  [m1]
[  m2  ]

case 6 - m2 joins, same end
[  m1 ]
   [m2]



case 1b - m2 entirely before m1
[m2] [m1]

case 2b - m2 handoff to m1
[m2 ]
    [ m1]

case 3b - m2 then overlap then m1
[m2 ]
   [ m1]

case 4b - same start, m2 leaves
[m2]
[m1   ]

case 5b - m1 contains m2
  [m2]
[  m1  ]

case 6b - m1 joins, same end
[m2   ]
   [m1]



case 13 - perfect overlap
[ m1 ]
[ m2 ]

