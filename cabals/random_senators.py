import numpy as np

import sys; import pathlib; sys.path.append(str(pathlib.Path(__file__).parent.parent))
from senate import cache

for member in cache.CacheObj.member_metadata:
  if member.party == 'D':
    color = '#40c4ff'
  elif member.party == 'R':
    color = '#ff5252'
  else:
    color = '#ea80fc'

  position = ' '.join(map(str, np.random.random(3)*2-1))
  text = f"{member.name} {member.party} {member.state}"
  print(position, color, text)

