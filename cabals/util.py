import time
import numpy as np

def rotateX(theta):
  c = np.cos(theta)
  s = np.sin(theta)
  return np.array([
    [1,  0, 0, 0],
    [0,  c, s, 0],
    [0, -s, c, 0],
    [0,  0, 0, 1]
  ], dtype='f4')

def rotateY(theta):
  c = np.cos(theta)
  s = np.sin(theta)
  return np.array([
    [c, 0, -s, 0],
    [0, 1,  0, 0],
    [s, 0,  c, 0],
    [0, 0,  0, 1]
  ], dtype='f4')

def second_format(duration):
  for unit in ['s', 'ms', 'us']:
    if duration < 1.0:
      duration *= 1000
    else:
      return (duration, unit)
  return (duration/1000, unit)

def timer(label):
  def wrapper(f):
    def wrapped(*args, **kwargs):
      print(label, end='... ', flush=True)
      t_start = time.time()
      f(*args, **kwargs)
      duration = time.time() - t_start
      duration, unit = second_format(duration)
      print(f"finished [{duration:.3f} {unit}]")
    return wrapped
  return wrapper
