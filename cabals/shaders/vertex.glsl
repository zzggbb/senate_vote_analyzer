uniform float size, eye_distance;
uniform vec2 dimensions;
uniform vec3 vertex;
uniform mat4 model_rotation;

in vec2 corner, a_tex_coord;
out vec2 v_tex_coord;

/*
+y|
  |
  |________+x
 /
/+z
*/
void main() {
  mat4 m_proj = perspective(90, dimensions.x/dimensions.y, 1e-5, 40);
  vec3 eye = vec3(0.0, 0.0, eye_distance);
  vec4 model_vertex = model_rotation * vec4(vertex, 1.0);
  mat4 m_view = lookat(eye, vec3(0.0, 0.0, -1.0), vec3(0.0,1.0,0.0));
  gl_Position = m_proj*m_view*(model_vertex + vec4(size*corner, 0, 0));
  v_tex_coord = a_tex_coord;
}
