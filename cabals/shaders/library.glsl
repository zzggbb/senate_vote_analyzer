#version 330
#define PI 3.14159265
mat4 perspective(float fovy, float aspect, float near, float far) {
  fovy = fovy*PI/180.0/2.0;
  float ct = 1 / tan(fovy);
  float rd = 1 / (far - near);
  return mat4(
    ct/aspect, 0,  0,               0,
    0,         ct, 0,               0,
    0,         0,  -(far+near)*rd, -1,
    0,         0,  -2*far*near*rd,  0
  );
}
mat4 lookat(vec3 eye, vec3 at, vec3 up) {
  vec3 f = normalize(at - eye);
  vec3 s = normalize(cross(f, up));
  vec3 u = normalize(cross(s, f));
  return mat4(
    vec4(s,0),vec4(u,0),vec4(-f,0),vec4(-dot(s,eye),-dot(u,eye),dot(f, eye),1.0)
  );
}
mat4 rotateX(float theta) {
  float c = cos(theta), s = sin(theta);
  return mat4(
    1, 0,  0, 0,
    0, c, -s, 0,
    0, s,  c, 0,
    0, 0, 0, 1
  );
}
mat4 rotateY(float theta) {
  float c = cos(theta), s = sin(theta);
  return mat4(
     c, 0, s, 0,
     0, 1, 0, 0,
    -s, 0, c, 0,
     0, 0, 0, 1
  );
}
vec3 encode_color(uint i) {
  uint r = (i & uint(0x0000FF)) >> 0;
  uint g = (i & uint(0x00FF00)) >> 8;
  uint b = (i & uint(0xFF0000)) >> 16;
  return vec3(float(r)/255.0, float(g)/255.0, float(b)/255.0);
}

