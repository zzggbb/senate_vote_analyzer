uniform sampler2D sampler;
uniform uint node_index;
uniform bool node_selection_mode;
in vec2 v_tex_coord;

void main() {
  vec3 color = node_selection_mode ? \
               encode_color(node_index) : texture(sampler, v_tex_coord).xyz;
  float d = distance(v_tex_coord, vec2(0.5));
  if (d > 0.5) discard;
  gl_FragColor = vec4(color, 1.0);
}
