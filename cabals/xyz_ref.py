import numpy as np
import itertools

for p in itertools.product([-1, 0, 1], repeat=3):
  p = np.array(p)
  float_c = (p * 127.5 + 127.5).astype(np.uint8)
  hex_c = ''.join(f"{c:02x}" for c in float_c)
  x, y, z = p
  print(f"node {x} {y} {z} 0.2 #{hex_c} #ffffff {x},{y},{z}")

print("edge 0 1 0.01 #ffffff")
print("edge 24 25 0.01 #ffffff")
