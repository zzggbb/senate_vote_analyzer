import sys
import struct
from pathlib import Path
from dataclasses import dataclass

import util

import moderngl_window as mglw
from moderngl_window.conf import settings
from moderngl_window.timers.clock import Timer

import numpy as np
from PIL import Image, ImageFont, ImageDraw

BASE_PATH = Path(__file__).parent
VERTEX_SHADER = BASE_PATH / 'shaders' / 'vertex.glsl'
FRAGMENT_SHADER = BASE_PATH / 'shaders' / 'fragment.glsl'
SHADER_LIBRARY = BASE_PATH / 'shaders' / 'library.glsl'
FONT_PATH = BASE_PATH / 'roboto.ttf'
SAMPLER_UNIT = 1
INDEX_RESERVED = 2**24 - 1

@dataclass
class Node:
  position: np.array
  size: float
  background_color: str
  label_color: str
  label: list[str]
  texture: mglw.moderngl.Texture = None

@dataclass
class Edge:
  index1: int
  index2: int
  thickness: float
  color: str

class GraphViewer:
  def __init__(self, input_lines):
    settings.WINDOW = {
      'class': 'moderngl_window.context.pyglet.Window',
      'gl_version': (3, 3),
      'window_size': (1000, 1000),
      'aspect_ratio': None,
      'title': 'float_me',
      'log_level': mglw.logging.WARN
    }
    self.wnd = mglw.create_window_from_settings()
    self.wnd.resize_func = self.resize
    self.wnd.iconify_func = self.iconify
    self.wnd.key_event_func = self.key_event
    self.wnd.mouse_position_event_func = self.mouse_position_event
    self.wnd.mouse_drag_event_func = self.mouse_drag_event
    self.wnd.mouse_scroll_event_func = self.mouse_scroll_event
    self.wnd.mouse_press_event_func = self.mouse_press_event
    self.wnd.mouse_release_event_func = self.mouse_release_event
    self.wnd.unicode_char_entered_func = self.unicode_char_entered
    self.wnd.close_func = self.close
    self.ctx = self.wnd.ctx
    self.ctx.enable_only(self.ctx.DEPTH_TEST)
    self.prog = self.ctx.program(
      vertex_shader = open(SHADER_LIBRARY).read() + open(VERTEX_SHADER).read(),
      fragment_shader = open(SHADER_LIBRARY).read() + open(FRAGMENT_SHADER).read()
    )
    self.read_input(input_lines)
    self.mouse_click_event = None
    self.set_uniform('node_selection_mode', False)
    self.background_color = (0.25, 0.25, 0.25)
    self.model_rotation = np.eye(4, dtype='f4')
    self.set_uniform('model_rotation', self.model_rotation.flatten())
    self.set_uniform('eye_distance', 0)
    self.controls_direction = -1
    self.set_uniform('sampler', SAMPLER_UNIT)
    self.generate_textures()

    # setup vertex array
    self.CORNER_BUFFER = self.ctx.buffer(np.array([
      -1, -1, # bottom left
      -1, 1,  # upper left
      1, -1,  # bottom right
      1, 1    # upper right
    ], dtype='f4'))
    self.TEXPOS_BUFFER = self.ctx.buffer(np.array([
      0, 1, # upper left
      0, 0, # bottom left
      1, 1, # upper right
      1, 0  # bottom right
    ], dtype='f4'))
    self.VAO = self.ctx.vertex_array(self.prog, [
      (self.CORNER_BUFFER, '2f', 'corner'),
      (self.TEXPOS_BUFFER, '2f', 'a_tex_coord')
    ])

  @util.timer('reading input list of points')
  def read_input(self, lines):
    self.NODES = []
    self.EDGES = []

    for line in lines:
      line_type, *line_data = line.split()
      if line_type == 'edge':
        try: index1, index2, thickness, color = line_data
        except ValueError: raise ValueError(f"edge definition '{line}' is invalid")
        self.EDGES.append(Edge(int(index1), int(index2), float(thickness), color))

      elif line_type == 'node':
        try: x, y, z, size, background_color, label_color, *label = line_data
        except ValueError: raise ValueError(f"node definition '{line}' is invalid")
        position = np.array([x, y, z], dtype='f4')
        size = float(size)
        self.NODES.append(Node(position, size, background_color, label_color, label))

      else:
        raise ValueError(f"input line '{line}' is not a valid node or edge definition")

    for edge in self.EDGES:
      for index in [edge.index1, edge.index2]:
        if not (0 <= index < len(self.NODES)):
          raise ValueError(f"edge definition contained invalid index: '{index}'")

    #self.EDGES_VAO = self.ctx.vertex_array(self.

  @util.timer("generating textures")
  def generate_textures(self):
    size = 256
    font = ImageFont.truetype(str(FONT_PATH), int(size/7.3))
    size = np.array([size, size])
    for node in self.NODES:
      image = Image.new("RGB", tuple(size), node.background_color)
      draw = ImageDraw.Draw(image)
      draw.text(
        tuple(size/2), '\n'.join(node.label), fill=node.label_color,
        font=font, anchor='mm', align='center'
      )
      texture = self.ctx.texture(size, 3, image.tobytes())
      texture.filter = (self.ctx.LINEAR, self.ctx.LINEAR)
      node.texture = texture

  def set_uniform(self, name, value):
    if name in self.prog: self.prog[name] = value

  def update_uniform(self, name, value, m=-np.inf, M=np.inf):
    if name in self.prog: self.prog[name] = np.clip(self.prog[name].value + value, m, M)

  def process_click(self):
    if not self.mouse_click_event:
      return
    self.set_uniform('node_selection_mode', True)
    self.ctx.clear(1.0, 1.0, 1.0)
    self.draw_nodes()
    x1, y1 = self.mouse_click_event
    viewport = (x1, self.ctx.screen.height - y1, 1, 1)
    pixel_bytes = self.ctx.screen.read(viewport=viewport, components=3)
    index = struct.unpack('<I', pixel_bytes + b'\0')[0]
    if (index != INDEX_RESERVED):
      print("You clicked on:", ' '.join(self.NODES[index].label))

    self.set_uniform('node_selection_mode', False)
    self.mouse_click_event = None

  def draw_nodes(self):
    for index, node in enumerate(self.NODES):
      self.set_uniform('node_index', index)
      self.set_uniform('vertex', node.position)
      self.set_uniform('size', node.size)
      node.texture.use(location=SAMPLER_UNIT)
      self.VAO.render(mode=self.ctx.TRIANGLE_STRIP)

  def draw_lines(self):
    #self.EDGES_VAO.render(mode=self.ctx.LINES)
    pass

  def render(self, time, frame_time):
    self.process_click()
    self.ctx.clear(*self.background_color)
    self.draw_lines()
    self.draw_nodes()

  def run(self):
    timer = Timer()
    timer.start()
    while not self.wnd.is_closing:
      self.wnd.clear()
      time, frame_time = timer.next_frame()
      self.render(time, frame_time)
      self.wnd.swap_buffers()
    self.wnd.destroy()

  def key_event(self, key, action, modifiers):
    keys = self.wnd.keys
    return
    print(f"KEY EVENT: {key=} {action=} {modifiers=}")

  def unicode_char_entered(self, char):
    pass

  def mouse_position_event(self, x, y, dx, dy):
    pass

  def mouse_drag_event(self, x, y, dx, dy):
    pitch = self.controls_direction * dy/500
    yaw = self.controls_direction * dx/500
    self.model_rotation = self.model_rotation @ util.rotateX(pitch) @ util.rotateY(yaw)
    self.set_uniform('model_rotation', self.model_rotation.flatten())

  def mouse_press_event(self, x, y, button):
    self.last_mouse_press_event = np.array([x, y, button])

  def mouse_release_event(self, x, y, button):
    if button == self.last_mouse_press_event[2] and \
        np.linalg.norm(self.last_mouse_press_event[:2] - np.array([x, y])) < 5:
      self.mouse_click_event = (x, y)
      self.last_mouse_press_event = None

  def mouse_scroll_event(self, x_offset, y_offset):
    self.update_uniform('eye_distance', 0.1*y_offset, 0.0, 5.0)
    self.controls_direction = -1 if self.prog['eye_distance'].value == 0.0 else 1

  def resize(self, width, height):
    self.set_uniform('dimensions', (width, height))

  def iconify(self, iconify):
    pass

  def close(self):
    self.CORNER_BUFFER.release()
    self.TEXPOS_BUFFER.release()
    self.VAO.release()
    for node in self.NODES:
      node.texture.release()

if __name__ == '__main__':
  lines = sys.stdin.read().splitlines()
  gv = GraphViewer(lines)
  gv.run()

