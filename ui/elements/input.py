from dataclasses import dataclass
from . import basic

@dataclass
class TextField(basic.TextBox):
  filled_color: str

  def __post_init__(self):
    super().__post_init__()
    self.empty_text = self.text
    self.empty_color = self.text_color

    self.set_empty()

  def set_empty(self):
    self.empty = True
    self.text = self.empty_text
    self.text_color = self.empty_color

  def set_filled(self):
    self.empty = False
    self.text = ''
    self.text_color = self.filled_color

  def match(self, text):
    return self.empty or self.text.lower() in text.lower()

  def process_char(self, char):
    if char == 'BACKSPACE':
      if self.empty: return
      self.text = self.text[:-1]
      if self.text == '':
        self.set_empty()
      self.update_elements()
    else:
      if self.empty:
        self.set_filled()
      self.text += char
      self.update_elements()

  def press(self, p):
    p = self.local(p)
    print(f'press within text field at {p}')

  def __repr__(self):
    return f'TextField {super().__repr__()}'
