COLORS = {
    'red': ('E57373', 'F44336', 'B71C1C'),
    'pink': ('F06292', 'E91E63', '880E4F'),
    'purple': ('BA68C8', '9C27B0', '4A148C'),
    'blue': ('64B5F6', '2196F3', '0D47A1'),
    'cyan': ('4DD0E1', '00BCD4', '006064'),
    'teal': ('4DB6AC', '009688', '004D40'),
    'green': ('81C784', '4CAF50', '1B5E20'),
    'yellow': ('FFF176', 'FFEB3B', 'FBC02D'),
    'orange': ('FFB74D', 'FF9800', 'E65100'),
    'main': ('E0E0E0', '9E9E9E', '212121')
}

def linscale(x, x1, x2, y1, y2):
  return (y2-y1)/(x2-x1)*(x-x2) + y2

def text_dimensions(text):
  lines = text.split('\n')
  # width is length of the longest line
  width = len(max(lines, key=len))
  # height is number of lines
  height = len(lines)
  return width, height

def calc_font_size(tw, th, kern, width, height, vertical=False):
  top = height/width
  if vertical:
    considering_width = 1.0/tw
    considering_height = top/(th*kern + kern/2)
  else:
    considering_width = 1.0/(tw*kern + kern/2)
    considering_height = top/th
  return min(considering_width, considering_height)

def encode_color(color_string):
  if not color_string:
    raise ValueError(color_string)

  if color_string[0] == '#' and len(color_string) == 7:
    hex_code = color_string[1:]
  else:
    if color_string[:5] == 'light':
      color_key, color_index = color_string[5:], 0
    elif color_string[:4] == 'dark':
      color_key, color_index = color_string[4:], 2
    else:
      color_key, color_index = color_string, 1
    hex_code = COLORS[color_key][color_index]

  return tuple(int(hex_code[i:i+2], 16)/255 for i in [0,2,4])

def factors(n):
  for i in range(1, n+1):
    if n % i == 0:
      yield (int(i), int(n/i))

def get_ticks_spec(n_data, max_ticks):
  if (n_data <= max_ticks):
    return (n_data, 1)

  factor_pairs = list(factors(n_data - 1))
  if len(factor_pairs) == 2:
    # the prime problem
    factor_pairs = list(factors(n_data - 2))

  for skip, n_ticks in factor_pairs:
    if n_ticks < max_ticks:
      return (n_ticks + 1, skip)
