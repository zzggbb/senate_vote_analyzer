from dataclasses import dataclass
from . import Box, TextBox, VerticalTextBox
from . import util
import numpy as np

@dataclass
class BarChart(Box):
  x_values: list[int | float]
  y_values: list[int | float]
  title: str
  x_axis_label: str
  y_axis_label: str
  alignment: str
  bar_color: str

  def alignment_flip(self, a, b):
    if self.alignment == 'vertical': return (a,b)
    elif self.alignment == 'horizontal': return (b,a)
  def __repr__(self):
    return f'BarChart "{self.title}" {super().__repr__()}'
  def __post_init__(self):
    super().__post_init__()
    if len(self.x_values) != len(self.y_values):
      raise ValueError("x-values and y-values must be the same length")
    text_color = "darkmain"
    axis_line_color = "main"
    N_x, N_y = len(self.x_values), len(self.y_values)
    min_y, max_y = min(self.y_values), max(self.y_values)
    min_x, max_x = min(self.x_values), max(self.x_values)
    title_height = 0.08
    gutter = 0.15
    gutter_ratio = 0.4
    axis_label_dim = gutter_ratio*gutter
    axis_ticks_dim = (1-gutter_ratio)*gutter
    T = 0.005 # axis line thickness
    max_ticks_h, max_ticks_v = 20, 10
    max_ticks_x, max_ticks_y = self.alignment_flip(max_ticks_h, max_ticks_v)

    n_data_h, n_data_v = self.alignment_flip(N_x, N_y)
    n_ticks_x, skip_factor = util.get_ticks_spec(N_x, max_ticks_x)
    n_ticks_h, n_ticks_v = self.alignment_flip(n_ticks_x, max_ticks_y)
    tick_label_width = (1.0-gutter)/(max_ticks_h)
    tick_label_height = self.top*(1.0 - title_height - gutter)/(max_ticks_v)
    graph_axis_width = 1.0 - gutter - tick_label_width
    graph_axis_height = (1.0-title_height-gutter)*self.top - tick_label_height
    graph_axis_length_major, graph_axis_length_minor = self.alignment_flip(
      graph_axis_height, graph_axis_width
    )

    if self.alignment == 'vertical':
      axis_ticks_h = np.linspace(min_x, max_x, n_ticks_h)
      axis_ticks_v = np.linspace(0 if min_y>0 else min_y, max_y, n_ticks_v)
    elif self.alignment == 'horizontal':
      axis_ticks_h = np.linspace(min_y, max_y, n_ticks_h)
      axis_ticks_v = np.linspace(min_x, max_x, n_ticks_v)[::-1]
    axis_text = self.alignment_flip(self.x_axis_label, self.y_axis_label)
    sign_prefix = self.alignment_flip(min_x < 0, min_y < 0)
    if min_y >= 0:
      graph_major_data = max_y
      midline = 0
    else:
      graph_major_data = -min_y + max_y
      midline = (-min_y)/graph_major_data * graph_axis_length_major

    bar_thickness = graph_axis_length_minor / N_x
    get_offset = lambda y_value: 0 if y_value >= 0 else y_value/graph_major_data*graph_axis_length_major
    axis_labels = ([f"{v:.1f}" for v in axis_ticks_h], [f"{v:.1f}" for v in axis_ticks_v])
    h_float_fmt = "+.1f" if sign_prefix[0] else ".1f"
    v_float_fmt = "+.3f" if sign_prefix[1] else ".3f"

    axis_line_x = gutter + tick_label_width/2 - T
    axis_line_y = gutter*self.top + tick_label_height/2 - T
    self.elements = [
      TextBox( # title
        0.0, self.top*(1.0-title_height), 1.0, title_height*self.top,
        self.color, True, None,
        self.title, 'auto', 'MM', text_color),
      Box( # vertical axis line
        axis_line_x, axis_line_y, T, graph_axis_height + T,
        axis_line_color, True, None),
      Box( # horizontal axis line
        axis_line_x, axis_line_y, graph_axis_width + T, T,
        axis_line_color, True, None),
      TextBox( # horizontal axis label
        axis_label_dim, 0.0, 1.0-axis_label_dim, axis_label_dim*self.top,
        self.color, True, None,
        axis_text[0], 'auto', 'MM', text_color),
      VerticalTextBox( # vertical axis label
        0.0, axis_label_dim*self.top, axis_label_dim, (1.0-title_height-axis_label_dim)*self.top,
        self.color, True, None,
        axis_text[1], 'auto', 'MM', text_color),
    ]

    for i, v in enumerate(axis_ticks_h):
      if self.alignment == 'vertical':
        offset = (i*skip_factor)*bar_thickness + bar_thickness/2 - tick_label_width/2
      elif self.alignment == 'horizontal':
        offset = i*tick_label_width - tick_label_width/2
      x = gutter + tick_label_width/2 + offset
      y = axis_label_dim*self.top
      w = tick_label_width
      h = axis_ticks_dim*self.top
      element = VerticalTextBox(x, y, w, h, "lightmain", True, None, f"{v:{h_float_fmt}}", 'auto', 'MM', text_color)
      self.elements.append(element)

    for i, v in enumerate(axis_ticks_v):
      x = axis_label_dim
      y = gutter*self.top + i*tick_label_height
      w = axis_ticks_dim
      h = tick_label_height
      element = TextBox(x, y, w, h, "lightmain", True, None, f"{v:{v_float_fmt}}", 'auto', 'ML', text_color)
      self.elements.append(element)

    for i, (x_value, y_value) in enumerate(zip(self.x_values, self.y_values)):
      if self.alignment == 'vertical':
        x = gutter + tick_label_width/2 + bar_thickness*i
        y = gutter*self.top + tick_label_height/2 + midline + get_offset(y_value)
        w = bar_thickness
        h = abs(y_value)/graph_major_data*graph_axis_length_major
      elif self.alignment == 'horizontal':
        x = gutter + tick_label_width/2 + T/2 + midline + get_offset(y_value)
        y = self.top*(1.0-title_height) - tick_label_height/2 - bar_thickness*(i+1)
        w = abs(y_value)/graph_major_data*graph_axis_length_major
        h = bar_thickness
      element = Box(x, y, w, h, self.bar_color, True, None)
      self.elements.append(element)
