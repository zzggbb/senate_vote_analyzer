from dataclasses import dataclass
from . import util, Box, TextBox, TextField
import numpy as np

@dataclass
class Table(Box):
  data: list[list[str]]
  alignments: list[str]

  def __post_init__(self):
    super().__post_init__()
    self.scroll_position = 0
    self.displayed_rows = 20
    self.controls = 0.1

    self.body = 1.0 - self.controls
    self.n_rows = len(self.data)
    self.n_cols = len(self.data[0])
    if len(self.alignments) != self.n_cols:
      raise ValueError("length of alignments must equal number of columns")

    # each row must be as tall as tallest item in the row
    self.row_heights = [0] * self.n_rows
    # each column must be as wide as the widest item in the column
    self.column_widths = [0] * self.n_cols
    for i in range(self.n_rows):
      if len(self.data[i]) != self.n_cols:
        raise ValueError("Each row in the data must be the same length")
      for j in range(self.n_cols):
        text_width, text_height = util.text_dimensions(self.data[i][j])
        if text_height > self.row_heights[i]:
          self.row_heights[i] = text_height
        if text_width > self.column_widths[j]:
          self.column_widths[j] = text_width

    self.total_height = sum(self.row_heights)
    self.total_width = sum(self.column_widths)

    # each column's font size must be the smallest font size in the column
    self.font_sizes = [np.inf] * self.n_cols
    for i in range(self.n_rows):
      for j in range(self.n_cols):
        text_width, text_height = util.text_dimensions(self.data[i][j])
        width = self.column_widths[j]/self.total_width
        height = self.row_heights[i]/self.displayed_rows
        font_size = util.calc_font_size(text_width, text_height, 0.5, width, height)
        if font_size < self.font_sizes[j]:
          self.font_sizes[j] = font_size

    self.tapped_element = None
    self.control_elements = [
      TextField(
        0.0, self.body*self.top, 0.4, 0.1*self.top,
        self.color, True, 'darkmain',
        'type something...', 'auto', 'ML', 'main',
        'darkmain'
      )
    ]

    self.update_elements()

  def update_elements(self):
    self.elements = []
    y = self.top*self.body
    matches = [
      (i, row) for i, row in enumerate(self.data)
      if self.control_elements[0].match(row[1])
    ]
    for i, row in matches[:self.displayed_rows]:
      x = 0
      h = self.top*self.body*self.row_heights[i]/self.displayed_rows
      for j in range(self.n_cols):
        w = self.column_widths[j]/self.total_width
        element = TextBox(
          x, y-h, w, h,
          self.color, True, 'darkmain',
          row[j], self.font_sizes[j], self.alignments[j], 'darkmain'
        )
        self.elements.append(element)
        x += w
      y -= h

    self.elements += self.control_elements

  def scroll(self, dx, dy):
    self.scroll_position =  np.clip(
      self.scroll_position + dy, 0, self.n_rows - self.displayed_rows
    )
    self.update_elements()

  def press(self, p):
    p = self.local(p)
    for element in self.control_elements:
      if element.overlap(p):
        self.tapped_element = element
        self.tapped_element.press(p)
        return
    self.tapped_element = None

  def process_char(self, char):
    if self.tapped_element:
      if hasattr(self.tapped_element, 'process_char'):
        self.tapped_element.process_char(char)
        self.update_elements()

  def __repr__(self):
    return f"Table {self.n_rows}x{self.n_cols} {super().__repr__()}"
