import sys
from dataclasses import dataclass

from . import util

import numpy as np

@dataclass
class Rectangle:
  x: float
  y: float
  w: float
  h: float
  def __post_init__(self):
    self.top = self.h/self.w

  def set_global(self, rect=None):
    if rect is None:
      # the global coordinates of a top-level element are the regular coordinates
      self.gx = self.x; self.gy = self.y; self.gw = self.w; self.gh = self.h
    else:
      self.gx = util.linscale(self.x, 0, 1,         rect.gx, rect.gx + rect.gw)
      self.gy = util.linscale(self.y, 0, rect.top,  rect.gy, rect.gy + rect.gh)
      self.gw = util.linscale(self.w, 0, 1,         0,       rect.gw)
      self.gh = util.linscale(self.h, 0, rect.top,  0,       rect.gh)

  def local(self, p):
    return np.array([
      util.linscale(p[0], self.x, self.x+self.w, 0, 1),
      util.linscale(p[1], self.y, self.y+self.h, 0, self.top)
    ])

  def overlap(self, p):
    px, py = p
    return (self.x < px < self.x + self.w) and (self.y < py < self.y + self.h)

  def __repr__(self):
    return f"L[{self.x:.3f} {self.y:.3f} {self.w:.3f}x{self.h:.3f}] G[{self.gx:.3f} {self.gy:.3f} {self.gw:.3f}x{self.gh:.3f}]"

@dataclass
class Box(Rectangle):
  color: str
  visible: bool
  outline: str
  def __post_init__(self):
    super().__post_init__()
    self.color_floats = util.encode_color(self.color)
    self.outline_color_floats = util.encode_color(self.outline) if self.outline else (0,0,0)
  def move(self, dx, dy):
    self.x += dx
    self.y += dy
  def __repr__(self):
    return f"Box bg=#{self.color} {super().__repr__()}"

@dataclass
class Character(Box):
  text_color: str
  char: str
  cw90: bool = False
  def __post_init__(self):
    super().__post_init__()
    self.text_color_floats = util.encode_color(self.text_color)
    self.char_index = ord(self.char)/255 if ' ' <= self.char <= '~' else 1.0
  def set_global(self, rect=None):
    if rect is None:
      self.gx = self.x; self.gy = self.y; self.gw = self.w; self.gh = self.h
    else:
      self.gx = util.linscale(self.x, 0, 1, rect.gx, rect.gx + rect.gw)
      self.gy = util.linscale(self.y, 0, rect.top, rect.gy, rect.gy + rect.gh)
      self.gw = self.w * rect.gw #util.linscale(self.w, 0, 1, 0,      rect.gw)
      self.gh = self.h * rect.gw #util.linscale(self.h, 0, 1, 0,      rect.gh)

  def __repr__(self):
    return f"Character '{self.char}' fg=#{self.text_color} {super().__repr__()}"

@dataclass
class TextBox(Box):
  text: str
  font_size: float | str
  align: str
  text_color: str

  def __post_init__(self):
    super().__post_init__()
    self.valign, self.halign = self.align
    self.kern = 0.5

    self.update_elements()

  def update_elements(self):
    self.lines = self.text.split('\n')
    text_width, self.text_height = util.text_dimensions(self.text)
    if self.font_size == 'auto':
      self.font_size = util.calc_font_size(text_width, self.text_height, self.kern, self.w, self.h)
    self.elements = []
    y = self.initial_y()
    for line in self.lines:
      x = self.initial_x(len(line))
      for char in line:
        element = Character(
          x, y, self.font_size, self.font_size,
          self.color, self.visible, None,
          self.text_color, char
        )
        self.elements.append(element)
        x += self.font_size*self.kern
      y -= self.font_size

  def initial_y(self):
    if self.valign == 'T': return self.top - self.font_size
    if self.valign == 'M': return self.top/2 + self.text_height*self.font_size/2 - self.font_size
    if self.valign == 'B': return 0.0 + self.text_height*self.font_size - self.font_size
    raise ValueError(f"invalid vertical alignment '{self.valign}'")

  def initial_x(self, line_width):
    if self.halign == 'L': return 0.0 - self.font_size*(1-self.kern)/2
    if self.halign == 'M': return 0.5 - line_width*self.font_size*self.kern/2 - self.font_size*(1-self.kern)/2
    if self.halign == 'R': return 1.0 - line_width*self.font_size*self.kern - self.font_size*(1-self.kern)/2
    raise ValueError(f"invalid horizontal alignment '{self.halign}'")

  def __repr__(self):
    return f'TextBox {repr(self.text)} fs={self.font_size:.2f} align={self.align} {super().__repr__()}'

@dataclass
class VerticalTextBox(Box):
  text: str
  font_size: float | str
  align: str # [T|M|B][L|M|R]
  text_color: str

  def __post_init__(self):
    super().__post_init__()
    self.valign, self.halign = self.align
    self.elements = []
    self.lines = self.text.split('\n')
    text_height, self.text_width = util.text_dimensions(self.text)
    self.kern = 0.5
    if self.font_size == 'auto':
      self.font_size = util.calc_font_size(self.text_width, text_height, self.kern, self.w, self.h, vertical=True)

    x = self.initial_x()
    for line in self.lines:
      y = self.initial_y(len(line))
      for char in line:
        element = Character(
          x, y, self.font_size, self.font_size,
          self.color, self.visible, None,
          self.text_color, char, True
        )
        self.elements.append(element)
        y -= self.font_size*self.kern
      x -= self.font_size

  def initial_y(self, line_height):
    if self.valign == 'T': return self.top - self.font_size
    if self.valign == 'M': return (self.top-self.font_size*line_height*self.kern)/2 + \
                                  self.font_size*self.kern*(line_height-1.5)
    if self.valign == 'B': return self.font_size*(line_height-1)*self.kern
    raise ValueError(f"invalid vertical alignment '{self.valign}'")

  def initial_x(self):
    if self.halign == 'L': return (self.text_width-1)*self.font_size
    if self.halign == 'M': return (1.0 - self.text_width*self.font_size)/2 + \
                                  (self.text_width-1)*self.font_size
    if self.halign == 'R': return 1.0 - self.font_size
    raise ValueError(f"invalid horizontal alignment '{self.halign}'")

  def __repr__(self):
    return f'TextBox {repr(self.text)} fs={self.font_size:.2f} align={self.align} {super().__repr__()}'
