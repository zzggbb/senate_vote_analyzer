class ElementTree:
  def __init__(self, elements):
    self.elements = elements

    # element under the cursor
    self.hovered_element = None
    # element under the cursor while cursor is pressed
    self.grabbed_element = None
    # element most recently clicked on
    self.tapped_element = None

    self.set_globals()

  def iterate(self, root=None):
    if root is None: root = self
    for element in getattr(root, 'elements', []):
      if element.visible:
        yield element
        for element in self.iterate(element):
          yield element

  def set_globals(self, root=None, rect=None):
    """
    Update global coordinates of all elements in the tree rooted at `root`
    """
    if root is None: root = self
    for element in getattr(root, 'elements', []):
      element.set_global(rect)
      self.set_globals(element, element)

  def hover(self, p):
    for element in self.elements:
      if element.visible and element.overlap(p):
        self.hovered_element = element
        return
    self.hovered_element = None

  def press(self, p):
    for element in self.elements:
      if element.overlap(p) and element.visible:
        self.grabbed_element = element
        self.tapped_element = element
        # propagate the event
        self.tapped_element.press(p)
        return

  def release(self):
    self.grabbed_element = None

  def scroll(self, dx, dy):
    if self.hovered_element:
      self.hovered_element.scroll(dx, dy)
      self.set_globals(self.hovered_element, self.hovered_element)

  def move(self, dx, dy):
    '''
    if self.selected_element is None:
      return

    self.selected_element.move(dx, dy)
    self.selected_element.set_global()
    self.set_globals(self.selected_element, self.selected_element)
    '''
    pass

  def process_char(self, char):
    if self.tapped_element:
      if hasattr(self.tapped_element, 'process_char'):
        self.tapped_element.process_char(char)
        self.set_globals(self.tapped_element, self.tapped_element)
