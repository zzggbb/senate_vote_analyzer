from context import test
from context import util

test(util.get_ticks_spec(21, 20), (11, 2))
test(util.get_ticks_spec(22, 20), (8, 3))
test(util.get_ticks_spec(23, 20), (12, 2))
test(util.get_ticks_spec(24, 20), (12, 2))
test(util.get_ticks_spec(25, 20), (13, 2))
test(util.get_ticks_spec(26, 20), (6, 5))
test(util.get_ticks_spec(27, 20), (14, 2))
test(util.get_ticks_spec(28, 20), (10, 3))
test(util.get_ticks_spec(29, 20), (15, 2))
test(util.get_ticks_spec(30, 20), (15, 2))
