from context import tree
from context import box
from context import test


print("Test 1")
class Z:
  pass
class Y:
  elements = [Z()]
class X:
  elements = [Y(), Z()]
t = tree.Tree([X()])
print(t)

print("\nTest 2")
class Z:
  def data(self):
    return [1,2,3]
class Y:
  elements = [Z(), Z()]
class X:
  elements = [Y()]
t = tree.Tree([X()])
print(t)
print(t.flatten('data'))

print("\nTest 3")
class Z:
  x = 1
  def __repr__(self):
    return f"x={self.x}"
  def adjust(self, R):
    self.x += R.dx
class Y:
  x = 5
  def __repr__(self):
    return f"x={self.x}"
  def adjust(self, R):
    self.x += R.dx
class X:
  dx = 7
  elements = [Y(), Z()]
  def __repr__(self):
    return f"dx={self.dx}"
t = tree.Tree([X()])
print(t)
at = t.adjusted()
print("Original", t)
print("Adjusted", at)
