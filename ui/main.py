# std imports
import sys
import logging
from pathlib import Path

# local imports
import elements
from element_tree import ElementTree
sys.path.append(str(Path(__file__).parent.parent)); from senate import cache

# 3rd party imports
import numpy as np
from PIL import Image, ImageFont, ImageDraw
import moderngl_window as mglw

BASE_PATH = Path(__file__).parent
VERTEX_SHADER = BASE_PATH / 'shaders' / 'vertex.glsl'
FRAGMENT_SHADER = BASE_PATH / 'shaders' / 'fragment.glsl'
INITIAL_DIMENSIONS = (1600, 1200)
FONT_PATH = BASE_PATH / 'font.png'
DEBUG = False

class UI(mglw.WindowConfig):
  gl_version = (3,3)
  window_size = INITIAL_DIMENSIONS
  aspect_ratio = None
  title = "float_me"
  #log_level = None

  def __init__(self, **kwargs):
    super().__init__(**kwargs)
    self.program = self.ctx.program(
      vertex_shader = open(VERTEX_SHADER).read(),
      fragment_shader = open(FRAGMENT_SHADER).read(),
    )
    font_texture = self.ctx.texture((1024,1024), 4, Image.open(FONT_PATH).tobytes())
    font_texture.use(location=0)
    self.setu('FONT', 0)
    self.vao = self.ctx.vertex_array(self.program, [
      (self.ctx.buffer(np.array([0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0, 0.0], dtype='f4')), '2f4', 'corner'),
    ])
    self.dimensions = INITIAL_DIMENSIONS
    self.update_dimensions()

    member_metadata_table = [
      [str(i), str(member.name), member.member_id, member.state,
       cache.CacheObj.party_trackers[member].get_history()]
      for i, member in enumerate(cache.CacheObj.member_metadata)
    ]

    self.element_tree = ElementTree([
      elements.Table(
        0.1, self.top*0.1, 0.8, self.top*0.8,
        "lightmain", True, None,
        member_metadata_table,
        ['MM', 'MR', 'MM', 'MM', 'ML'],
      ),

      elements.BarChart(
        0.0, self.top/2, 0.5, self.top/2,
        "lightmain", False, None,
        np.arange(1,50), np.arange(1,50)**2, "graph of f(x)=x^2",
        "x axis", "y axis", "vertical", "lightblue"
      ),

      elements.BarChart(
        0.0, 0.0, 1.0, self.top,
        "lightmain", False, None,
        np.linspace(0,10,300), np.sin(np.linspace(0,10,300)), "graph of f(x)=sin(x/5)",
        "x axis", "y axis", "vertical", "lightgreen"),

      elements.BarChart(
        0.0, 0.0, 0.5, self.top/2,
        "lightmain", False, None,
        np.arange(200), np.random.random(200)*10 - 5, "graph of random values between -5 and 5",
        "x axis", "y axis", "horizontal", "teal"),

      elements.BarChart(
        0.5, 0.0, 0.5, self.top/2,
        "lightmain", False, None,
        range(1,4), np.random.randint(1,10,3), "rachel's favorite numbers",
        "zaaaane", "ragel", "vertical", "lightpurple"
      )
    ])
    if DEBUG:
      for element in self.element_tree.iterate():
        if not isinstance(element, elements.Character):
          print(element)

  def setu(self, name, value):
    if name in self.program: self.program[name] = value

  def screen_coord(self, x, y):
    d = np.array(self.dimensions)
    return np.array([x, y]) / d[0]

  def update_dimensions(self):
    self.setu('screen_dimensions', self.dimensions)
    self.top = self.dimensions[1]/self.dimensions[0]

  def render(self, time, frame_time):
    self.ctx.clear(0.0, 0.0, 0.0)
    for element in self.element_tree.iterate():
      self.setu('element_bottom_left', (element.gx, element.gy))
      self.setu('element_dimensions', (element.gw, element.gh))
      self.setu('cw90', getattr(element, 'cw90', False))
      self.setu('element_color', element.color_floats)
      self.setu('element_text_color', getattr(element, 'text_color_floats', (0, 0, 0)))
      self.setu('element_char_index', getattr(element, 'char_index', -1.0))
      self.setu('outline_enabled', True if element.outline else False)
      self.setu('outline_color', element.outline_color_floats)
      self.vao.render(self.ctx.TRIANGLE_FAN)

  def resize(self, width, height):
    self.dimensions = (width, height)
    self.update_dimensions()

  def mouse_position_event(self, x, y, dx, dy):
    y = self.dimensions[1] - y
    dy = -dy
    p = self.screen_coord(x, y)
    self.element_tree.hover(p)

  def mouse_drag_event(self, x, y, dx, dy):
    y = self.dimensions[1] - y
    dy = -dy
    self.element_tree.move(dx/self.dimensions[0], dy/self.dimensions[0])

  def mouse_scroll_event(self, x_offset, y_offset):
    self.element_tree.scroll(x_offset, y_offset)

  def mouse_press_event(self, x, y, button):
    y = self.dimensions[1]-y
    p = self.screen_coord(x, y)
    if button == 1:
      self.element_tree.press(p)

  def mouse_release_event(self, x, y, button):
    self.element_tree.release()

  def key_event(self, key, action, modifiers):
    if action == self.wnd.keys.ACTION_PRESS:
      if ord(' ') <= key <= ord('~'):
        char = chr(key)
      elif key == 65288:
        char = 'BACKSPACE'
      else:
        #raise RuntimeError(f"unhandled key {key}")
        return
      self.element_tree.process_char(char)

    elif action == self.wnd.keys.ACTION_RELEASE:
      pass

if __name__ == '__main__':
  UI.run()
