#version 330
uniform sampler2D FONT;
uniform vec2 screen_dimensions;
uniform vec2 element_dimensions;
uniform vec3 element_color;
uniform vec3 element_text_color;
uniform float element_char_index;
uniform bool outline_enabled;
uniform vec3 outline_color;
in vec2 v_tex_coord;
out vec4 color;

float linscale(float x, float x1, float x2, float y1, float y2) {
  return (y2-y1)/(x2-x1)*(x-x2) + y2;
}

vec4 get_font_texture(vec2 uv) {
  return texture(FONT, vec2(uv.x, 1.0-uv.y));
}

void main() {
  if (element_char_index < 0) {
    color = vec4(element_color.rgb, 1.0);
  } else {
    float blur = 0.009;
    vec2 char_bottom_left = vec2(
      linscale(mod(element_char_index*255, 16), 0, 15, 0, 0.9375),
      linscale(floor(element_char_index*255/16), 0, 15, 0.9375, 0.0)
    );
    float d = get_font_texture(char_bottom_left + v_tex_coord / 16.0).a;
    if (d > 0.5+blur)
      discard;
    else
      color = vec4(mix(element_text_color, element_color, smoothstep(0.5-blur,0.5+blur,d)), 1.0);
  }
  if (outline_enabled) {
    float _thickness = 1.0;
    vec2 thickness = _thickness / (element_dimensions * screen_dimensions);
    if ((v_tex_coord.x < thickness.x) || (v_tex_coord.x > 1.0-thickness.x) ||
        (v_tex_coord.y < thickness.y) || (v_tex_coord.y > 1.0-thickness.y))
      color = vec4(outline_color, 1.0);
  }
}
