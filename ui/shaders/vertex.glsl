#version 330
uniform vec2 screen_dimensions;
uniform vec2 element_bottom_left;
uniform vec2 element_dimensions;
uniform bool cw90;
in vec2 corner;
out vec2 v_tex_coord;

void main(){
  vec2 p = element_bottom_left + element_dimensions*corner;
  p.x = p.x*2.0 - 1.0;
  p.y = p.y*2.0*screen_dimensions.x/screen_dimensions.y - 1.0;
  gl_Position = vec4(p, 0.0, 1.0);
  if (cw90) {
    v_tex_coord = vec2(float(!bool(corner.y)), corner.x);
  } else {
    v_tex_coord = corner;
  }
}
