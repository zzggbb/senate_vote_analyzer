class MemberName:
  def __init__(self, name):
    self.first, self.last = name.split(' ', 1)

  def __lt__(self, other):
    return self.last < other.last

  def __str__(self):
    return f"{self.first} {self.last}"

  def __repr__(self):
    return str(self)

class MemberMetadata:
  def __init__(self, party, state, member_id, name):
    self.party = party
    self.state = state
    self.member_id = member_id
    self.name = MemberName(name)

  def match(self, first_name_filter, last_name_filter, state_filter, party_filter):
    if all([
      first_name_filter is None or first_name_filter.lower() in self.name.first.lower(),
      last_name_filter is None or last_name_filter.lower() in self.name.last.lower(),
      state_filter is None or state_filter == self.state,
      party_filter is None or party_filter == self.party,
    ]):
      return True

  def short_repr(self, show_party=True, show_state=True):
    line = f"[{self.member_id}"
    state_party = []
    if show_party: state_party.append(self.party)
    if show_state: state_party.append(self.state)
    if state_party:
      line += ' ' + '-'.join(state_party)
    line += f'] {self.name}'
    return line

  def long_repr(self, index):
    return f"Index: {index}\nMember ID: {self.member_id}\nName: {self.name}\n" + \
           f"State: {self.state}"

  def __format__(self, format_spec):
    # TODO: implement
    return f"{self.short_repr():{format_spec}}"

  def __hash__(self):
    return int(self.member_id[1:])

  def __eq__(self, other):
    return hash(self) == hash(other)

  def __lt__(self, other):
    return hash(self) < hash(other)

  def __iter__(self):
    return iter([self.member_id, self.party, self.state, self.name])

  def __repr__(self):
    return self.short_repr()

  def __str__(self):
    return self.short_repr()
