from dataclasses import dataclass
from .cache import CacheObj

class PartyTracker:
  def __init__(self, party, vote_index):
    self.history = [(party, vote_index)]

  def check(self, party, vote_index):
    if party != self.history[-1][0]:
      self.history.append((party, vote_index))

  def __repr__(self):
    return ' -> '.join(
        f"{party} ({CacheObj.vote_metadata[vote_index]})" \
        for party, vote_index in self.history
    )

  def get_history(self):
    return '\n'.join(
        f"{party} ({CacheObj.vote_metadata[vote_index]})" \
        for party, vote_index in self.history
    )

  def switched(self):
    return len(self.history) > 1

class Game:
  def __init__(self, first):
    self.term = Term(first, 0)
    self.entered = []
    self.exited = []

  def __repr__(self):
    return f"{len(self.term)} {self.term} entered:{self.entered} exited:{self.exited}"

class GameTracker:
  def __init__(self, n_votes):
    self.games = [Game(0)]
    self.n_votes = n_votes

  def add_enter(self, vote_index, member_index):
    if self.games[-1].term.first != vote_index:
      self.games[-1].term.last = vote_index-1
      self.games.append(Game(vote_index))

    self.games[-1].entered.append(member_index)

  def add_exit(self, vote_index, member_index):
    if self.games[-1].term.first != vote_index:
      self.games[-1].term.last = vote_index-1
      self.games.append(Game(vote_index))

    self.games[-1].exited.append(member_index)

  def finalize(self):
    self.games[-1].term.last = self.n_votes-1

  def print_games(self):
    print()
    for i, game in enumerate(self.games):
      print(i, game)

@dataclass
class Term:
  first: int # vote index of first vote
  last: int # vote index of last vote

  def overlap(self, other):
    if other is None:
      return self
    start = max(self.first, other.first)
    stop = min(self.last, other.last)
    if start > stop:
      return None
    return Term(start, stop)

  def to_slice(self):
    return slice(self.first, self.last+1)

  def __len__(self):
    return self.last - self.first + 1

  def __str__(self):
    return f"{CacheObj.vote_metadata[self.first]} ... "+\
           f"{CacheObj.vote_metadata[self.last]}"

  def __gt__(self, other):
    return self.first > other.last

  def __hash__(self):
    return hash(str(self))

class TermTracker:
  def __init__(self, n_votes):
    self.n_votes = n_votes
    self.terms = []
    self.in_office = False
    self.last_entrance = None

  def add_enter(self, index):
    self.in_office = True
    self.last_entrance = index

  def add_exit(self, index):
    self.in_office = False
    self.terms.append(Term(self.last_entrance, index))
    self.last_entrance = None

  def normalize(self):
    if self.in_office:
      return self.terms + [Term(self.last_entrance, self.n_votes-1)]
    else:
      return self.terms

  def max_term(self):
    term_lengths = [len(term) for term in self.normalize()]
    return max(term_lengths)

  def overlaps(self, other):
    overlaps = []
    for term1 in self.normalize():
      for term2 in other.normalize():
        overlap = term1.overlap(term2)
        if overlap:
          overlaps.append(overlap)
    return overlaps

  def __repr__(self):
    return '[' + ' '.join(str(term) for term in self.normalize()) + ']'

  def __len__(self):
    return len(self.normalize())

  def __getitem__(self, i):
    return self.normalize()[i]

class Adjacents:
  def __init__(self):
    self.p = []
    self.s = []

  def add(self, p, s):
    self.p.append(p)
    self.s.append(s)

  def __iter__(self):
    return iter([self.p ,self.s])

@dataclass
class Occupation:
  member_index: int # who did the occupation?
  term: Term        # when did they occupy?

  def __repr__(self):
    name = CacheObj.member_metadata[self.member_index].name
    return f"{name} ({self.term})"

  def __str__(self):
    return repr(self)

  def __gt__(self, other):
    return self.term > other.term

@dataclass
class Seat:
  occupations: list[Occupation]

  def add(self, occupation):
    self.occupations.append(occupation)

  def most_recent(self):
    return self.occupations[-1]

  def __repr__(self):
    return ' > '.join(map(str, self.occupations))

  def __len__(self):
    return len(self.occupations)

  def __getitem__(self, i):
    return self.occupations[i]

@dataclass
class StateSeats:
  state: str
  seat_a: Seat
  seat_b: Seat

  def add(self, occupation):
    if occupation > self.seat_a.most_recent():
      self.seat_a.add(occupation)
    elif occupation > self.seat_b.most_recent():
      self.seat_b.add(occupation)

  def __repr__(self):
    return (f"{self.state}: {self.seat_a}\n    {self.seat_b}")

  def __iter__(self):
    return iter([self.seat_a, self.seat_b])
