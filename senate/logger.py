LOGGING = True
def log(msg, end='\n'):
  if LOGGING:
    print(msg, end=end, flush=True)
