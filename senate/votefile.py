import textwrap
from datetime import datetime

from . import const
from . import member
from . import util

class VoteMetadata:
  def __init__(self, header, timestamp, description, result):
    header_items = header.split()
    self.congress, self.session, self.year, self.vote_number, \
    self.yeas, self.nays, self.absent, self.present = \
    map(int, header_items[:-1])
    self.threshold_str = header_items[-1]
    self.threshold_float = eval(self.threshold_str)
    self.timestamp = datetime.strptime(timestamp, "%B %d, %Y,  %I:%M %p")
    self.description = description
    self.result = result
    if result in const.VOTE_PASS:
      self.passed = True
      self.passed_text = 'passed'
    elif result in const.VOTE_FAIL:
      self.passed = False
      self.passed_text = 'failed'
    else:
      self.passed = None
      self.passed_text = "??? unknown vote result '{result}' ???"

  def match(self, year_filter, number_filter, threshold_filter, result_filter):
    return all([
      util.filter_match(year_filter, self.year),
      util.filter_match(number_filter, self.vote_number),
      util.filter_match(result_filter, self.passed),
      util.filter_match(threshold_filter, self.threshold_str)
    ])

  def short_repr(self, show_description):
    line = f"[{self.congress}-{self.session} {self.year}-{self.vote_number:03}]"+\
           f"{self.yeas:>3} {self.nays:>3} {self.absent:>2} {self.present:>2} | "+\
           f"{self.threshold_str} | {self.passed_text} | {self.result}"
    if show_description:
      line += '\n' + textwrap.indent(textwrap.fill(self.description, 80), '  ')
    return line

  def long_repr(self):
    description = textwrap.indent(textwrap.fill(self.description, width=80), ' '*6)
    output = f"""\
    Congress {self.congress} - Session {self.session} - Vote {self.vote_number}
    Date: {self.timestamp}
    Description:
    {description}
    Threshold: {self.threshold_str}
    Result: {self.result}
    Vote Counts: Y:{self.yeas} N:{self.nays} A:{self.absent} P:{self.present}\
    """
    return textwrap.dedent(output)

  def __repr__(self):
    date = self.timestamp.strftime("%m/%d/%Y")
    return f"{date}-{self.vote_number}"

  def __hash__(self):
    return hash(f"{self.year}{self.vote_number}")

class VoteFile:
  def __init__(self, path):
    self.path = path
    with open(path, 'r') as file:
      try:
        header, timestamp, description, \
          result, *member_lines = file.read().splitlines()
      except ValueError:
        print(f"error: vote file '{path}' has a bad format")

    self.metadata = VoteMetadata(header, timestamp, description, result)
    self.members = {}
    for member_line in map(MemberLine, member_lines):
      self.members[member_line.member] = member_line

  def __str__(self):
    return f"{self.metadata.year}/{self.metadata.vote_number}"

  def __repr__(self):
    return str(self)

  def __lt__(self, other):
    if self.metadata.year > other.metadata.year:
      return False
    if self.metadata.year < other.metadata.year:
      return True
    if self.metadata.year == other.metadata.year:
      return self.metadata.vote_number < other.metadata.vote_number

class MemberLine:
  def __init__(self, line):
    party, state, position, member_id, name = line.split(' ', 4)
    self.member = member.MemberMetadata(party, state, member_id, name)
    self.position = position
