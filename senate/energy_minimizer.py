from .vector import Vector

class EnergyMinimizer:
  def __init__(self, entities, ideal_distances, dimensions=3):
    self.dimensions = dimensions
    self.n_entities = len(entities)
    self.positions = {
      entity: Vector(dimensions=dimensions).random() for entity in entities
    }

    self.distances = {
      (e1, e2): [
        ideal_distance,
        self.positions[e1].distance(self.positions[e2])
      ] for (e1, e2), ideal_distance in ideal_distances.items()
    }

  def __str__(self):
    line = ' '.join(f'{k}{v}' for k, v in self.positions.items()) + ' | '
    line += ' '.join(f'{e1}-{e2}({d[0]:.2f} {d[1]:.2f})' for (e1, e2), d in self.distances.items())
    return line

  def step(self):
    Kp = 0.75

    # calculate net velocity of each entity
    net_velocity = {
      entity: Vector(dimensions=self.dimensions) for entity in self.positions
    }
    for (e1, e2), (ideal, current) in self.distances.items():
      error = ideal - current
      velocity = (self.positions[e1]-self.positions[e2]).unit() * error * 0.5 * Kp
      net_velocity[e1] += velocity
      net_velocity[e2] += -velocity

    # move entities
    for entity in self.positions:
      self.positions[entity] += net_velocity[entity]

    # recalculate self.distances
    for (e1, e2) in self.distances:
      self.distances[(e1, e2)][1] = self.positions[e1].distance(self.positions[e2])
