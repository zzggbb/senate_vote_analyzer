SHOW_VOTES = '''
Print information about votes.

\b
Vote Range Specification:
  Y1 Y2           All votes between vote #1 of year Y1 and the last vote of year Y2
  Y1.x Y2         All votes between vote #x of year Y1 and the last vote of year Y2
  Y1 Y2.x         All votes between vote #1 of year Y1 and vote #x of year Y2
  Y1.a Y2.b       All votes between vote #a of year Y1 and vote #b of year Y2
\b
Vote Range Examples:
  2000 2003           All votes from 2000, 2001, 2002, and 2003.
  2009.17 2012        All votes between vote #17 in 2009 and the last vote of 2012.
  2003 2005.10        All votes between vote #1 in 2003 and vote #10 in 2005.
  1996.15 1998.50     All votes between vote #15 in 1996 and vote #50 in 1998.
'''

MODES = """\b
0: compact mode (default)
1: positions sorted by last name
2: positions grouped by position
3: positions grouped by state
4: positions grouped by party
"""
