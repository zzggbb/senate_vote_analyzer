# local imports
from . import const
from .logger import log

# 3rd party imports
import numpy as np

class Cache:
  def __init__(self):
    self.path_cache = {}

  @staticmethod
  def check_directory():
    if const.CACHE_PATH.is_dir():
      log("Using existing cache")
    else:
      log("Cache does not exist, creating it now")
      const.CACHE_PATH.mkdir()

  def load(self, path):
    if path.is_file():
      if path not in self.path_cache:
        self.path_cache[path] = np.load(path, allow_pickle=True)
      return self.path_cache[path]
    return None

  @property
  def total_votes_by_year(self):
    return self.load(const.TOTAL_VOTES_BY_YEAR).tolist()
  @total_votes_by_year.setter
  def total_votes_by_year(self, year_to_total_votes):
    np.save(const.TOTAL_VOTES_BY_YEAR, year_to_total_votes)

  @property
  def member_metadata(self):
    return self.load(const.MEMBER_METADATA)
  @member_metadata.setter
  def member_metadata(self, metadata):
    np.save(const.MEMBER_METADATA, metadata)

  @property
  def vote_metadata(self):
    return self.load(const.VOTE_METADATA)
  @vote_metadata.setter
  def vote_metadata(self, metadata):
    np.save(const.VOTE_METADATA, metadata)

  @property
  def positions(self):
    return self.load(const.POSITIONS)
  @positions.setter
  def positions(self, positions):
    np.save(const.POSITIONS, positions)

  @property
  def party_trackers(self):
    return self.load(const.PARTY_TRACKERS).tolist()
  @party_trackers.setter
  def party_trackers(self, party_trackers):
    np.save(const.PARTY_TRACKERS, party_trackers)

  @property
  def term_trackers(self):
    return self.load(const.TERM_TRACKERS)
  @term_trackers.setter
  def term_trackers(self, term_trackers):
    np.save(const.TERM_TRACKERS, np.array(term_trackers, dtype=object))

  @property
  def state_seats(self):
    return self.load(const.STATE_SEATS).tolist()
  @state_seats.setter
  def state_seats(self, state_seats):
    np.save(const.STATE_SEATS, state_seats)

  @property
  def member_adjacents(self):
    return self.load(const.MEMBER_ADJACENTS).tolist()
  @member_adjacents.setter
  def member_adjacents(self, member_adjacents):
    np.save(const.MEMBER_ADJACENTS, member_adjacents)

  @property
  def agreements(self):
    return self.load(const.AGREEMENTS)
  @agreements.setter
  def agreements(self, graph):
    np.save(const.AGREEMENTS, graph)

CacheObj = Cache()
