import pathlib

VOTE_THRESHOLDS = ['1/2', '3/5', '2/3']

VOTE_POSITION_MAP = {
  'Yea': 'Y',
  'Nay': 'N',
  'Not Voting': 'A',
  'Present, Giving Live Pair': 'L',
  'Present': 'P',
  'Guilty': 'Y',
  'Not Guilty': 'N',
}

VOTE_POSITION_ENCODING = {
  # zero means the member was not in office during the vote
  'Y': 1,
  'N': 2,
  'A': 3,
  'L': 4,
  'P': 5,
}

VOTE_POSITION_DECODING = [
  None, 'Y', 'N', 'A', 'L', 'P'
]

VOTE_PASS = [
  'Amendment Agreed to',
  'Nomination Confirmed',
  'Motion to Table Agreed to',
  'Cloture Motion Agreed to',
  'Motion Agreed to',
  'Bill Passed',
  'Conference Report Agreed to',
  'Motion to Proceed Agreed to',
  'Joint Resolution Passed',
  'Resolution Agreed to',
  'Cloture on the Motion to Proceed Agreed to',
  'Motion for Attendance Agreed to',
  'Concurrent Resolution Agreed to',
  'Resolution of Ratification Agreed to',
  'Motion to Discharge Agreed to',
  'Motion to Table Motion to Recommit Agreed to',
  'Veto Overridden',
  'Motion to Reconsider Agreed to',
  'Decision of Chair Sustained',
  'Guilty',
  'Amendment Germane',
  'Point of Order Sustained',
  'Motion to Adjourn Agreed to',
  'Motion to Table Motion to Reconsider Agreed to',
  'Point of Order Well Taken'
]

VOTE_FAIL = [
  'Amendment Rejected',
  'Nomination Rejected',
  'Motion to Table Failed',
  'Cloture Motion Rejected',
  'Motion Rejected',
  'Bill Defeated',
  'Conference Report Rejected',
  'Motion to Proceed Rejected',
  'Joint Resolution Defeated',
  'Resolution Rejected',
  'Cloture on the Motion to Proceed Rejected',
  'Concurrent Resolution Rejected',
  'Resolution of Ratification Rejected',
  'Motion to Discharge Rejected',
  'Veto Sustained',
  'Motion to Recommit Rejected',
  'Motion to Reconsider Rejected',
  'Decision of Chair Not Sustained',
  'Not Guilty',
  'Amendment Not Germane',
  'Point of Order Not Sustained',
  'Motion to Adjourn Rejected',
  'Motion to Table Motion to Reconsider Rejected',
  'Point of Order Not Well Taken',
  'Motion to Postpone Rejected',
  'Objection Not Sustained',
  'Motion to Refer Rejected'
]

DATE_FORMAT = '%B %d, %Y, %I:%M %p'

CACHE_DIRECTORY = 'cache'
CACHE_PATH = pathlib.Path(__file__).parent.parent / CACHE_DIRECTORY

TOTAL_VOTES_BY_YEAR = CACHE_PATH / 'total_votes_by_year.npy'
MEMBER_METADATA = CACHE_PATH / 'member_metadata.npy'
VOTE_METADATA = CACHE_PATH / 'vote_metadata.npy'
POSITIONS = CACHE_PATH / 'positions.npy'
PARTY_TRACKERS = CACHE_PATH / 'party_trackers.npy'
TERM_TRACKERS = CACHE_PATH / 'term_trackers.npy'
STATE_SEATS = CACHE_PATH / 'state_seats.npy'
MEMBER_ADJACENTS = CACHE_PATH / 'member_adjacents.npy'
AGREEMENTS = CACHE_PATH / 'agreements.npy'

SYSTEM_STATUS = CACHE_PATH / 'system_status.npy'

STATE_CODES = {
  'AL': 'ALABAMA',
  'AK': 'ALASKA',
  'AZ': 'ARIZONA',
  'AR': 'ARKANSAS',
  'CA': 'CALIFORNIA',
  'CO': 'COLORADO',
  'CT': 'CONNECTICUT',
  'DE': 'DELAWARE',
  'FL': 'FLORIDA',
  'GA': 'GEORGIA',
  'HI': 'HAWAII',
  'ID': 'IDAHO',
  'IL': 'ILLINOIS',
  'IN': 'INDIANA',
  'IA': 'IOWA',
  'KS': 'KANSAS',
  'KY': 'KENTUCKY',
  'LA': 'LOUISIANA',
  'ME': 'MAINE',
  'MD': 'MARYLAND',
  'MA': 'MASSACHUSETTS',
  'MI': 'MICHIGAN',
  'MN': 'MINNESOTA',
  'MS': 'MISSISSIPPI',
  'MO': 'MISSOURI',
  'MT': 'MONTANA',
  'NE': 'NEBRASKA',
  'NV': 'NEVADA',
  'NH': 'NEW HAMPSHIRE',
  'NJ': 'NEW JERSEY',
  'NM': 'NEW MEXICO',
  'NY': 'NEW YORK',
  'NC': 'NORTH CAROLINA',
  'ND': 'NORTH DAKOTA',
  'OH': 'OHIO',
  'OK': 'OKLAHOMA',
  'OR': 'OREGON',
  'PA': 'PENNSYLVANIA',
  'RI': 'RHODE ISLAND',
  'SC': 'SOUTH CAROLINA',
  'SD': 'SOUTH DAKOTA',
  'TN': 'TENNESSEE',
  'TX': 'TEXAS',
  'UT': 'UTAH',
  'VT': 'VERMONT',
  'VA': 'VIRGINIA',
  'WA': 'WASHINGTON',
  'WV': 'WEST VIRGINIA',
  'WI': 'WISCONSIN',
  'WY': 'WYOMING'
}
