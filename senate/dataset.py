# standard library imports
import os
import shutil
from pathlib import Path
import itertools
import multiprocessing
import datetime

# local imports
from . import util
from . import const
from . import models
from . import scraper
from .logger import log
from .votefile import VoteFile, MemberLine
from .cache import Cache, CacheObj
from .pipeline import PipelineObj

# 3rd party imports
import numpy as np
np.seterr(invalid='ignore', divide='ignore')

def get_years():
  return sorted([d.name for d in const.CACHE_PATH.iterdir() if d.is_dir()])

def get_years_range():
  years = list(CacheObj.total_votes_by_year.keys())
  return years[0], years[-1]

def show_vote(vote_index, mode, show_description):
  member_metadata = CacheObj.member_metadata
  vote = CacheObj.vote_metadata[vote_index]
  positions = CacheObj.positions[vote_index]

  if mode == 0:
    print(vote.short_repr(show_description))
    return

  print(vote.long_repr())

  voters = []
  by_position = {}
  by_state = {}
  by_party = {}
  for member_index, member_position in enumerate(positions):
    if member_position == 0:
      continue

    member_obj = member_metadata[member_index]
    data = (member_obj, const.VOTE_POSITION_DECODING[member_position])
    voters.append(data) # mode 0
    util.create_append(by_position, member_position, data) # mode 1
    util.create_append(by_state, member_obj.state, data) # mode 2
    util.create_append(by_party, member_obj.party, data) # mode 3

  if mode == 1:
    sorted_data = sorted(voters, key=lambda e: e[0].name)
    rows = [f"{member} {position}" for member, position in sorted_data]
    print(util.columnize(rows, 3))

  elif mode == 2:
    for i, vote_label in enumerate(const.VOTE_POSITION_ENCODING, 1):
      if i in by_position:
        sorted_data = sorted(by_position[i], key=lambda e: e[0].name)
        print(vote_label, len(sorted_data))
        rows = [member.short_repr() for member,_ in sorted_data]
        print(util.columnize(rows, 3))
      else:
        print(vote_label, 0)

  elif mode == 3:
    sorted_states = sorted(by_state.items(), key=lambda e: e[0])
    for state, state_members in sorted_states:
      members = [f"{member.short_repr(show_state=False)} {position}" \
          for member, position in state_members]
      print(f"{state:<10} {members[0]:<40} {members[1]}")

  elif mode == 4:
    for party, party_members in by_party.items():
      party_members.sort(key=lambda e: e[0].name)
      print(party, len(party_members))
      rows = [f"{member.short_repr(show_party=False)} {position}" \
          for member, position in party_members]
      print(util.columnize(rows, 3))

def get_term(a, b):
  split_char = '.'
  def get_vote_index(year, vote_number):
    totals = sorted(CacheObj.total_votes_by_year.items())
    first_year = totals[0][0]
    return sum(totals[i][1] for i in range(year - first_year))+vote_number-1

  ytotals = dict(CacheObj.total_votes_by_year)
  if not a:
    via = vib = None

  elif a and not b:
    y, *vn = list(map(int, a.split(split_char)))
    via = get_vote_index(y, vn[0] if vn else 1)
    vib = via + (0 if vn else ytotals[y]-1)

  elif a and b:
    ya, *vna = list(map(int, a.split(split_char)))
    yb, *vnb = list(map(int, b.split(split_char)))
    via = get_vote_index(ya, vna[0] if vna else 1)
    vib = get_vote_index(yb, vnb[0] if vnb else ytotals[yb])

  if via is None: via = 0
  if vib is None: vib = len(CacheObj.vote_metadata)
  return models.Term(via, vib)

def identify_new_votes():
  missing_votes = {}
  year_to_total_votes = scraper.get_year_to_total_votes()
  for year, total in year_to_total_votes.items():
    year_path = const.CACHE_PATH / str(year)
    missing_votes[year] = []
    for vote_number in range(1, total+1):
      filepath = year_path / str(vote_number)
      if not filepath.is_file() or os.path.getsize(filepath) == 0:
        missing_votes[year].append(vote_number)

  CacheObj.total_votes_by_year = year_to_total_votes
  return missing_votes

def download_votes(new_votes, dry=False):
  for year, votes in new_votes.items():
    if not votes:
      continue

    year_path = const.CACHE_PATH / str(year)
    if not year_path.is_dir(): year_path.mkdir()

    vote_plural = 'votes' if len(votes) > 1 else 'vote'
    summary = util.summarize(votes)
    log(f"  {year}: Downloading {vote_plural} #{summary}... ", end='')

    unpublished_votes = []
    successful_votes = []
    for vote in votes:
      vote_data = scraper.get_vote(year, vote)
      if vote_data is None:
        unpublished_votes.append(vote)
      else:
        if not dry:
          open(year_path / str(vote), 'w').write(vote_data)
        successful_votes.append(vote)

    if successful_votes:
      summary = util.summarize(successful_votes)
      log(f"Successfully downloaded {vote_plural} #{summary}")

    if unpublished_votes:
      isnt_plural = "aren't" if len(unpublished_votes) > 1 else "isn't"
      summary = util.summarize(unpublished_votes)
      log(f", {vote_plural} #{summary} {isnt_plural} published yet")

@PipelineObj.add
def merge():
  vote_metadata = []
  member_metadata = set()
  party_trackers = {}

  vote_files = sorted(map(VoteFile, const.CACHE_PATH.glob('*/*')))

  for vote_index, vote_file in enumerate(vote_files):
    vote_metadata.append(vote_file.metadata)
    member_metadata.update(vote_file.members)
    for member in vote_file.members:
      if member not in party_trackers:
        party_trackers[member] = models.PartyTracker(member.party, vote_index)
      party_trackers[member].check(member.party, vote_index)

  member_metadata = sorted(member_metadata)

  positions = np.zeros((len(vote_files), len(member_metadata)), dtype='uint8')
  for i, vote_file in enumerate(vote_files):
    for j, member in enumerate(member_metadata):
      if member in vote_file.members:
        position_string = vote_file.members[member].position
        positions[i,j] = const.VOTE_POSITION_ENCODING[position_string]

  CacheObj.member_metadata = member_metadata
  CacheObj.vote_metadata = vote_metadata
  CacheObj.positions = positions
  CacheObj.party_trackers = party_trackers

@PipelineObj.add
def generate_term_trackers():
  positions = CacheObj.positions
  member_metadata = CacheObj.member_metadata
  vote_metadata = CacheObj.vote_metadata
  n_votes, n_senators = positions.shape
  term_trackers = []
  #game_tracker = models.GameTracker(n_votes)

  for j in range(n_senators):
    tracker = models.TermTracker(n_votes)
    if positions[0][j] != 0: tracker.add_enter(0)
    term_trackers.append(tracker)

  games = [models.Game(0)]
  for (i, [vote_a, vote_b]) in [(i, positions[i:i+2]) for i in range(n_votes-1)]:
    entered = []
    exited = []
    for j in range(n_senators):
      if vote_a[j] == 0 and vote_b[j] != 0:
        term_trackers[j].add_enter(i+1)
        entered.append(j)

      if vote_a[j] != 0 and vote_b[j] == 0:
        term_trackers[j].add_exit(i)
        exited.append(j)

    if entered or exited:
      games[-1].term.last = i
      new_game = models.Game(i+1)
      new_game.entered += entered
      new_game.exited += exited
      games.append(new_game)

  games[-1].term.last = n_votes - 1

  print()
  for i, game in enumerate(games):
    print(i, game)

  CacheObj.term_trackers = term_trackers

@PipelineObj.add
def generate_agreements(term_filter=None):
  positions = CacheObj.positions
  term_trackers = CacheObj.term_trackers
  n_votes, n_members = positions.shape
  agreements = np.zeros((4, n_members, n_members), dtype=np.float32)

  member_pairs = itertools.combinations(range(n_members), 2)
  for pair_index, (member_a, member_b) in enumerate(member_pairs):
    tracker_a = term_trackers[member_a]
    tracker_b = term_trackers[member_b]
    member_overlaps = tracker_a.overlaps(tracker_b)
    for member_overlap in member_overlaps:
      votes = member_overlap.overlap(term_filter)
      if votes is None:
        continue
      vote_slice = votes.to_slice()
      votes_a = positions[vote_slice, member_a]
      votes_b = positions[vote_slice, member_b]
      agreements[0, member_a, member_b] += len(votes_a)
      agreements[1, member_a, member_b] += np.sum(votes_a == votes_b)

  '''
  agreements:
  0 (S): total votes that both members took part in
  1 (A): total votes that both members agreed on
  2 (P): metric 1: percentage of agreement (A/S)
  3 (A2S) metric 2: (A^2)/S
  '''
  agreements[2,:,:] = np.where(
    agreements[0,:,:] == 0,
    0.0,
    agreements[1,:,:]/agreements[0,:,:]
  )
  agreements[3,:,:] = agreements[2,:,:] * agreements[1,:,:]

  if term_filter is None:
    CacheObj.agreements = agreements
  else:
    return agreements

@PipelineObj.add
def generate_state_seats():
  term_trackers = CacheObj.term_trackers
  member_metadata = CacheObj.member_metadata
  state_to_occupations = {}
  for member_index, tracker in enumerate(term_trackers):
    state = member_metadata[member_index].state
    if state not in state_to_occupations:
      state_to_occupations[state] = []
    for term in tracker.normalize():
      state_to_occupations[state].append(models.Occupation(member_index, term))

  state_to_seats = {}
  for state, occupations in sorted(state_to_occupations.items()):
    occ_1, occ_2, *occ_rest = sorted(occupations, key=lambda occ: occ.term.first)
    state_seats = models.StateSeats(state, models.Seat([occ_1]), models.Seat([occ_2]))
    for occupation in occ_rest:
      state_seats.add(occupation)
    state_to_seats[state] = state_seats

  CacheObj.state_seats = state_to_seats

@PipelineObj.add
def generate_member_adjacents():
  state_seats = CacheObj.state_seats
  adjacents = {}
  for state, state_seats in state_seats.items():
    for seat in state_seats:
      for i in range(len(seat)):
        current_member_index = seat[i].member_index
        if current_member_index not in adjacents:
          adjacents[current_member_index] = models.Adjacents()

        if len(seat) - 1 == 0:
          adjacents[current_member_index].add(None, None)
        elif i == 0:
          adjacents[current_member_index].add(None, seat[i+1])
        elif i == len(seat) - 1:
          adjacents[current_member_index].add(seat[i-1], None)
        else:
          adjacents[current_member_index].add(seat[i-1], seat[i+1])

  for adjacents_obj in adjacents.values():
    key_func = lambda occupation: 0 if occupation is None else occupation.member_index
    adjacents_obj.p.sort(key=key_func)
    adjacents_obj.s.sort(key=key_func)

  CacheObj.member_adjacents = adjacents

def get_system_status():
  return np.load(const.SYSTEM_STATUS, allow_pickle=True)

def set_system_status():
  n_votes = len(CacheObj.vote_metadata)
  n_members = len(CacheObj.member_metadata)
  timestamp = datetime.datetime.now()
  system_status = [n_votes, n_members, timestamp]
  np.save(const.SYSTEM_STATUS, system_status)

def update(dry, pipeline_filter):
  if pipeline_filter:
    PipelineObj.run(pipeline_filter)
    return

  if not scraper.check_connection():
    return

  Cache.check_directory()
  log(f"Searching for new votes from {scraper.DOMAIN}", end="... ")
  new_votes = identify_new_votes()
  if util.empty(new_votes):
    log("No new votes.")
  else:
    log("Found the following:")
    download_votes(new_votes, dry=dry)
    if dry:
      return
    PipelineObj.run(pipeline_filter)
    set_system_status()

def clean():
  if not const.CACHE_PATH.is_dir():
    log("the cache does not exist")
    return
  shutil.rmtree(const.CACHE_PATH)
  log("removed the cache")

