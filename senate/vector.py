import math
import random

class DimensionMismatch(Exception):
  pass

class Vector:
  def __init__(self, elements=None, dimensions=3):
    if elements:
      self.dimensions = len(elements)
      self.elements = elements
    else:
      self.dimensions = dimensions
      self.elements = [0.0]*dimensions

  def random(self):
    return Vector([random.random() for _ in range(self.dimensions)])

  def distance(self, other):
    if self.dimensions != other.dimensions:
      raise DimensionMismatch(self.dimensions, other.dimensions)

    return math.sqrt(sum((s - o)**2 for s, o in zip(self, other)))

  def length(self):
    return math.sqrt(sum(e**2 for e in self))

  def unit(self):
    return self / self.length()

  def __iter__(self):
    for e in self.elements:
      yield e

  def __add__(self, other):
    return Vector([s+o for s,o in zip(self, other)])

  def __sub__(self, other):
    return Vector([s-o for s,o in zip(self, other)])

  def __neg__(self):
    return Vector() - self

  def __mul__(self, x):
    return Vector([s*x for s in self])

  def __truediv__(self, x):
    return Vector([s/x for s in self])

  def __str__(self):
    string = ' '.join(f'{float(e):.2f}' for e in self)
    return f'({string})'

  def __repr__(self):
    return str(self)

