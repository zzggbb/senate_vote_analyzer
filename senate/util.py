import numpy as np

def nc2(n):
  return (n*n - n) // 2

def chunks(L, n):
  chunks = []
  size = int(np.ceil(len(L) / n))
  for i in range(n):
    offset = i*size
    chunks.append(L[offset:offset+size])
  return chunks

def filter_match(filter_value, value):
  return filter_value is None or value == filter_value

def empty(dictionary):
  return all(len(array)==0 for key, array in dictionary.items())

def create_append(dictionary, key, element):
  if key not in dictionary:
    dictionary[key] = []
  dictionary[key].append(element)

def columnize(data, n_columns):
  data = list(data)
  N = len(data)
  cut = int(np.ceil(N / n_columns))
  columns = [data[i*cut:np.clip(i*cut+cut, None, N)] for i in range(n_columns)]
  widths = [max(len(row) for row in column) if column else 0 for column in columns]
  return '\n'.join(
    ' '.join(
      columns[col][row].ljust(widths[col]+1) if row+col*cut<N \
      else '' for col in range(n_columns)
    ) for row in range(cut)
  )

def summarize(series):
  series = sorted(series)
  ranges = []
  start = series[0]
  stop = None
  for i in range(1, len(series)):
    curr = series[i]
    prev = series[i-1]
    if curr == prev + 1:
      stop = curr
    else:
      ranges.append([start, stop])
      start = curr
      stop = None
  ranges.append([start, stop])
  return ', '.join(f'{a}-{b}' if b else str(a) for a, b in ranges)

class Mirror:
  def __init__(self):
    self.index_to_object = {}
    self.object_to_index = {}
    self.index = 0

  def add(self, obj):
    self.index_to_object[self.index] = obj
    self.object_to_index[obj] = self.index
    self.index += 1

  def __getitem__(self, e):
    d = self.index_to_object if isinstance(e, int) else self.object_to_index
    if e not in d:
      raise IndexError
    return d[e]

  def __contains__(self, e):
    d = self.index_to_object if isinstance(e, int) else self.object_to_index
    return e in d

  def __len__(self):
    return len(self.index_to_object)

  def __repr__(self):
    return ', '.join(f'{i}-{o}' for i, o in self.index_to_object.items())
