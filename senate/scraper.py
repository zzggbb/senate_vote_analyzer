# standard library imports
import xml.etree.ElementTree

# local imports
from . import const
from .logger import log

# 3rd party imports
import requests
import lxml.html

DOMAIN = "www.senate.gov"
URL = f"https://{DOMAIN}/legislative/"
GET_VOTES = "LIS/roll_call_lists/vote_menu_{c}_{s}.xml"
GET_VOTE = "LIS/roll_call_votes/vote{c}{s}/vote_{c}_{s}_{v}.xml"

def year_to_session(year):
  x = (year - 1789)/2 + 1
  return (int(x), 2 if x % 1 == 0.5 else 1)

def session_to_year(congress, session):
  return 1786 + congress*2 + session

def xml_to_plain(xml_string):
  tree = xml.etree.ElementTree.fromstring(xml_string)
  header = [
    tree.find('congress').text,
    tree.find('session').text,
    tree.find('congress_year').text,
    tree.find('vote_number').text,
    tree.find('count').find('yeas').text,
    tree.find('count').find('nays').text,
    tree.find('count').find('absent').text or '0',
    tree.find('count').find('present').text or '0',
    tree.find('majority_requirement').text
  ]
  out = ' '.join(header) + '\n' + tree.find('vote_date').text + '\n'
  description = tree.find('vote_document_text').text or \
                tree.find('vote_question_text').text
  out += description + '\n' + tree.find('vote_result').text + '\n'
  for member in tree.find('members').findall('member'):
    # strip to remove leading and trailing spaces that the senate.gov
    # mistakenly puts in
    name = member.find('first_name').text.strip() + ' ' + \
          member.find('last_name').text.strip()
    name = name.replace('"', '')
    out += ' '.join([
      member.find('party').text,
      member.find('state').text,
      const.VOTE_POSITION_MAP[member.find('vote_cast').text],
      member.find('lis_member_id').text,
      name
    ]) + '\n'
  return out

def check_connection():
  # function executes in ~0.31 seconds
  try:
    response = requests.get(URL)
    if response.status_code == 200:
      log(f"Successfully connected to {DOMAIN}")
      return True
    elif response.status_code == 403:
      log(f"Access denied to {DOMAIN}")
      return False
  except requests.exceptions.ConnectionError:
    log(f"Error: Could not connect to {DOMAIN}")
    return False

def get_total_votes(year):
  # function executes in ~0.13 seconds
  congress, session = year_to_session(year)
  url = URL + GET_VOTES.format(c=congress, s=session)
  tree = xml.etree.ElementTree.fromstring(requests.get(url).text)
  return len(tree.find('votes').findall('vote'))

def get_published_years():
  # function executes in ~0.10 seconds
  url = URL + "votes_new.htm"
  response = requests.get(url)
  tree = lxml.html.fromstring(response.text)
  elements = tree.xpath('//form[@name="PastVotes"]/select/option')
  years = [int(element.text.split()[0]) for element in elements[1:]]
  # the current year is not shown in the same html element as the rest
  current_url = tree.xpath('//a[contains(text(), "Detailed Session List")]/@href')[0]
  current_congress, current_session = \
      current_url.split('/')[-1].split('.')[0].split('_')[2:]
  current_year = session_to_year(int(current_congress), int(current_session))
  #return [current_year] + years
  return years[::-1] + [current_year]

def get_year_to_total_votes():
  # function executes in ~4.10 seconds
  return {year: get_total_votes(year) for year in get_published_years()}

def get_vote(year, vote_number):
  # function executes in ~0.11 seconds
  vote_number = str(vote_number).zfill(5)
  congress, session = year_to_session(year)
  url = URL + GET_VOTE.format(c=congress, s=session, v=vote_number)
  response = requests.get(url, allow_redirects=False)
  return None if response.status_code == 301 else xml_to_plain(response.text)
