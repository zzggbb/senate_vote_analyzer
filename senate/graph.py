class Graph:
  def __init__(self):
    self.adjacency = {}

  def set_edge(self, a, b, e):
    '''
    Add an edge with value `e` between vertex `a` and vertex `b`
    '''
    if a not in self.adjacency:
      self.adjacency[a] = {}
    if b not in self.adjacency:
      self.adjacency[b] = {}

    self.adjacency[a][b] = e
    self.adjacency[b][a] = e

  def get_edge(self, a, b, default=None):
    '''
    If vertex `a` and vertex `b` share an edge, return the edge's value.
    Otherwise, return `default` [defaults to None]
    '''
    if a in self.adjacency and b in self.adjacency[a]:
      return self.adjacency[a][b]
    return default

  def neighbors(self, a):
    return self.adjacency[a]

