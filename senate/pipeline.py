import timeit
from .logger import log

class Pipeline:
  def __init__(self):
    self.stages = []

  def add(self, stage_function):
    self.stages.append(stage_function)
    return stage_function

  def run(self, stage_filter):
    log(f"Running pipeline...")
    time_start = timeit.default_timer()
    n_stages = len(self.stages)
    for stage_index, stage in enumerate(self.stages):
      if stage_filter not in [-1, stage_index+1]:
        continue
      progress = f"{stage_index+1}/{n_stages}"
      log(f"  Running stage {progress} '{stage.__name__}'", end='... ')
      time_stage = timeit.default_timer()
      stage()
      log(f"done [{timeit.default_timer()-time_stage:.3f}s]")
    log(f"Finished pipeline [{timeit.default_timer()-time_start:.3f}(s)]")

  def __repr__(self):
    return '->'.join(stage.__name__ for stage in self.stages)

PipelineObj = Pipeline()
