from context import vector

v1 = vector.Vector()
assert v1.d == 3
assert v1.v == [0.0, 0.0, 0.0]

v2 = vector.Vector(dimensions=2)
assert v2.d == 2
assert v2.v == [0.0, 0.0]

v3 = vector.Vector([0.0, 1.0, 2.0, 3.0])
assert v3.d == 4
assert v3.v == [0.0, 1.0, 2.0, 3.0]
