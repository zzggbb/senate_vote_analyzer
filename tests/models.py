from context import models
from context import test

# see notes/overlap.txt

term1 = models.Term(10, 20)
cases = {
  models.Term(30, 40): None, # case 1
  models.Term(20, 30): models.Term(20, 20), # case 2
  models.Term(18, 25): models.Term(18, 20), # case 3
  models.Term(10, 30): models.Term(10, 20), # case 4
  models.Term(5, 25):  models.Term(10, 20), # case 5
  models.Term(18, 20): models.Term(18, 20), # case 6
  models.Term(10, 20): models.Term(10, 20), # case 7
  models.Term(20, 20): models.Term(20, 20)
}
for term2, expected in cases.items():
  test(term1.overlap(term2), expected)
  test(term2.overlap(term1), expected)

tracker1 = models.TermTracker(10)
tracker1.add_enter(2); tracker1.add_exit(5)
tracker2 = models.TermTracker(10)
tracker2.add_enter(7); tracker2.add_exit(9)
test(tracker1.overlaps(tracker2), [])

tracker1 = models.TermTracker(10)
tracker1.add_enter(1); tracker1.add_exit(5)
tracker2 = models.TermTracker(10)
tracker2.add_enter(2); tracker2.add_exit(7)
test(tracker1.overlaps(tracker2), [models.Term(2, 5)])

tracker1 = models.TermTracker(10)
tracker1.add_enter(1); tracker1.add_exit(3)
tracker1.add_enter(7); tracker1.add_exit(9)
tracker2 = models.TermTracker(10)
tracker2.add_enter(2); tracker2.add_exit(8)
test(tracker1.overlaps(tracker2), [models.Term(2,3), models.Term(7,8)])
