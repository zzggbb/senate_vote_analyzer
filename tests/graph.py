from context import graph

G = graph.Graph()
G.set_edge('node1', 'node2', 5)
G.set_edge('node1', 'node3', 7)

assert G.get_edge('node1', 'node2') == 5
assert G.get_edge('node2', 'node1') == 5

assert G.get_edge('node1', 'node3') == 7
assert G.get_edge('node3', 'node1') == 7

assert G.get_edge('node2', 'node3') == None
assert G.get_edge('node3', 'node2') == None

assert len(G.neighbors('node1')) == 2
assert ('node2', 5) in G.neighbors('node1').items()
assert ('node3', 7) in G.neighbors('node1').items()

assert len(G.neighbors('node2')) == 1
assert ('node1', 5) in G.neighbors('node2').items()

assert len(G.neighbors('node3')) == 1
assert ('node1', 7) in G.neighbors('node3').items()
