import sys; import pathlib; sys.path.append(str(pathlib.Path(__file__).parent.parent))

from senate import dataset
from senate import vector
from senate import energy_minimizer
from senate import graph
from senate import scraper
from senate import logger
from senate import util
from senate import models
from senate import cache

def test(a, b):
  assert a == b, f"Fail: a={a} b={b}"
