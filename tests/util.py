from context import util
from context import test

L = list(range(10))
test(util.chunks(L, 3), [[0,1,2,3],[4,5,6,7],[8,9]])
test(util.chunks(L, 4), [[0,1,2],[3,4,5],[6,7,8],[9]])
test(util.chunks(L, 5), [[0,1],[2,3],[4,5],[6,7],[8,9]])
