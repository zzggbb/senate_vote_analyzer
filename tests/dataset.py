from context import dataset
from context import models
from context import cache

from context import test

# full
n_votes = sum(cache.CacheObj.total_votes_by_year.values())
test(dataset.get_term(None, None), models.Term(0, n_votes))

test(dataset.get_term('1989.1', '1989.2'), models.Term(0, 1))
test(dataset.get_term('1989', '1990'), models.Term(0, 637))
test(dataset.get_term('1989', None), models.Term(0, 311))
test(dataset.get_term('1989.1', None), models.Term(0, 0))
