from context import energy_minimizer

def three_body_test():
  print("three body test")
  entities = ['a', 'b', 'c']
  ideal_distances = {
    ('a', 'b'): 0.5,
    ('a', 'c'): 0.5,
    ('b', 'c'): 0.5,
  }

  system = energy_minimizer.EnergyMinimizer(entities, ideal_distances, dimensions=2)
  print(system)

  for _ in range(10):
    system.step()
    print(system)

def four_body_test():
  print("four body test")
  entities = ['a', 'b', 'c', 'd']
  ideal_distances = {
    ('a', 'b'): 0.5,
    ('b', 'c'): 0.5,
    ('c', 'd'): 0.5,
    ('d', 'a'): 0.5
  }

  system = energy_minimizer.EnergyMinimizer(entities, ideal_distances, dimensions=2)
  print(system)

  for _ in range(10):
    system.step()
    print(system)

three_body_test()
four_body_test()
