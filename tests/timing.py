import timeit
from context import scraper
from context import dataset
from context import logger

logger.LOGGING = False

def loop_timer(loops, func, *args, iarg=None):
  print(f"{func.__module__}.{func.__name__}:",
        end=' ', flush=True)
  t0 = timeit.default_timer()
  if iarg:
    for i in range(loops):
      func(*args, iarg(i))
  else:
    for i in range(loops):
      func(*args)
  t1 = timeit.default_timer()
  average = (t1-t0)/loops
  print(f"{average:.3f}(s)", flush=True)

loop_timer(10, scraper.check_connection)
loop_timer(10, scraper.get_total_votes, iarg=lambda i: 1989+i)
loop_timer(10, scraper.get_published_years)
loop_timer(5, scraper.get_year_to_total_votes)
loop_timer(10, scraper.get_vote, 1991, iarg=lambda i: i+1)

loop_timer(5, dataset.merge)
loop_timer(5, dataset.generate_terms)
loop_timer(5, dataset.generate_state_seats)
loop_timer(5, dataset.generate_member_adjacents)
