@main.group("dataset", help="Manage the dataset.")
def dataset_group():
  pass

@dataset_group.command(help="Update the dataset.")
@click.option('-d', '--dry', is_flag=True, help="Don't actually download anything.")
@click.option('-p', '--pipeline', 'pipeline_only', is_flag=True, help="Only run the pipeline.")
def update(dry, pipeline_only):
  dataset.update(dry, pipeline_only)

@dataset_group.command(help="Remove the dataset.")
def clean():
  dataset.clean()

@dataset_group.command(help="Show the dataset system status.")
def status():
  n_votes, n_members, timestamp = dataset.get_system_status()
  print(f"Total Votes: {n_votes}")
  print(f"Total Members: {n_members}")
  print(f"Last Updated: {timestamp}")
