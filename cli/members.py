@main.group(help="View member information.")
@click.option('-f', '--first-name', help=\
              "Filter only members whose first name contains TEXT. Not case sensitive.")
@click.option('-l', '--last-name', help=\
              "Filter only members whose last name contains TEXT. Not case sensitive.")
@click.option('-s', '--state', type=click.Choice(const.STATE_CODES.keys()), help=\
              "Filter only members representing the given state. Must be a two-letter \
              state code, like WA, NY, CA, etc.", metavar='STATE')
@click.option('-p', '--party', type=click.Choice('IDR'), help=\
              "Filter only members from the given party.")
@click.option('-i', '--index', type=int, help="Select a member by their index.")
@click.pass_context
def members(ctx, first_name, last_name, state, party, index):
  ctx.obj['matching_members'] = []
  for member_index, member in enumerate(cache.CacheObj.member_metadata):
    if util.filter_match(index, member_index) and \
       member.match(first_name, last_name, state, party):
      ctx.obj['matching_members'].append((member_index, member))

@members.command('show', help="Print information about members.")
@click.pass_context
def members_show(ctx):
  vote_metadata = cache.CacheObj.vote_metadata
  member_metadata = cache.CacheObj.member_metadata
  term_trackers = cache.CacheObj.term_trackers
  party_trackers = cache.CacheObj.party_trackers
  member_adjacents = cache.CacheObj.member_adjacents
  for member_index, member in ctx.obj['matching_members']:
    tracker = term_trackers[member_index]
    predecesors, successors = member_adjacents[member_index]
    print(member.long_repr(member_index))
    print(f"Party: {party_trackers[member]}")
    for term_index in range(len(tracker)):
      incumbent = " (incumbent):" if tracker.in_office else ":"
      term = tracker[term_index]
      if term.first == 0:
        predecesor = "(unknown, cache doesn't include pre-1989 data)"
      else:
        predecesor = member_metadata[predecesors[term_index].member_index]
      print(f"Term {term_index+1}{incumbent}")
      print("  First Vote:", vote_metadata[term.first])
      print("  Last Vote:", vote_metadata[term.last])
      print("  Total Votes:", len(term))
      print("  Predecesor:", predecesor)
      if not tracker.in_office:
        print("  Successor:", member_metadata[successors[term_index].member_index])
    print()

@members.command(help="Show a list of parties that each member has been a part of.")
@click.option('-s', '--switched', 'switched_only', is_flag=True,
              help="Only show members who have switched parties.")
@click.pass_context
def party_history(ctx, switched_only):
  trackers = cache.CacheObj.party_trackers
  for member_index, member in ctx.obj['matching_members']:
    tracker = trackers[member]
    if switched_only and tracker.switched() or not switched_only:
        print(f"{member:<35}", tracker)
