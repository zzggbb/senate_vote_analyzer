@main.group(help="View vote information.")
@click.option('-l', '--last', 'last_n', type=int, metavar="N",
              help="Filter only the N most recent votes.")
@click.option('-r', '--range', '_range', nargs=2, metavar="START STOP", default=(None, None),
              help="Filter only votes between the given start and stop.")
@click.option('-y', '--year', 'year_filter', type=int, metavar="YEAR",
              help="Filter only votes from the given year.")
@click.option('-n', '--number', 'number_filter', type=int, metavar="NUMBER",
              help="Filter only votes with the given vote number.")
@click.option('-t', '--threshold', 'threshold_filter', metavar="THRESHOLD",
              type=click.Choice(const.VOTE_THRESHOLDS),
              help="Filter only votes that required the given threshold to pass.")
@click.option('--passed/--failed', 'result_filter', default=None,
              help="Filter only votes that passed/failed. By default, votes are " +
                   "shown regardless of their outcome.")
@click.pass_context
def votes(ctx, last_n, _range, year_filter, number_filter, threshold_filter, result_filter):
  if last_n:
    vote_slice = slice(-last_n, None)
  else:
    vote_slice = dataset.get_term(*_range).to_slice()
  ctx.obj['matching_votes'] = []
  for vote_index, vote in list(enumerate(cache.CacheObj.vote_metadata))[vote_slice]:
    if vote.match(year_filter, number_filter, threshold_filter, result_filter):
      ctx.obj['matching_votes'].append((vote_index, vote))

@votes.command('show', short_help="Print information about votes.", help=docs.SHOW_VOTES)
@click.option('-m', '--mode', metavar='MODE', default=0, show_default=True, type=click.IntRange(0,4),
              help=docs.MODES)
@click.option('-d', '--description', 'show_description', is_flag=True, default=False,
              help="Show vote descriptions. By default descriptions are hidden.")
@click.pass_context
def votes_show(ctx, mode, show_description):
  for vote_index, vote in ctx.obj['matching_votes']:
      dataset.show_vote(vote_index, mode, show_description)

@votes.command(help="Print a histogram of vote results")
@click.pass_context
def result_histogram(ctx):
  results = {}
  for vote_index, vote in ctx.obj['matching_votes']:
    if vote.result not in results:
      results[vote.result] = 0
    results[vote.result] += 1
  for result, count in sorted(results.items(), key=lambda p: p[1], reverse=True):
    print(str(count).rjust(5), result)

@votes.command(help="Print a histogram of vote scores")
@click.pass_context
def score_histogram(ctx):
  scores = {score: 0 for score in range(101)}
  for vote_index, vote in ctx.obj['matching_votes']:
    scores[vote.yeas] += 1

  bins = list(range(101))
  S = np.array(sorted(scores.items(), key=lambda p: p[0]))
  sorted_scores = sorted(scores.items(), key=lambda p: p[1], reverse=True)
  print(util.columnize([f"{s}: {c}" for s, c in sorted_scores], 12))
  heights = S.T[1]
  colors = [str(.75 - .75*greyscale**2) for greyscale in heights/max(heights)]
  plt.bar(bins, heights)
  plt.xticks(bins[::2], bins[::2])
  plt.show()
