import sys

from matplotlib import pyplot as plt

lines = sys.stdin.read().splitlines()
x = [int(line.split()[0]) for line in lines]
y = [int(line.split()[2]) for line in lines]

fig, ax = plt.subplots()
p = ax.bar(x, y, tick_label=x)
ax.bar_label(p)
plt.show()
