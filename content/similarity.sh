#!/bin/sh

arg1="$1"
arg2="$2"
tmp=/tmp/senate_similarity_${arg1}_${arg2}
python3 senate.py neighbors ${arg1} ${arg2} > ${tmp}.dot
dot -Tpng ${tmp}.dot > ${tmp}.png
feh ${tmp}.png
