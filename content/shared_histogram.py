import matplotlib
import matplotlib.pyplot as plt

from senate import dataset

graph = dataset.read_member_graph_cache()
shared_votes = [link.total_shared_votes for link in graph.values()]

bins = [0, 1, 10, 20, 50, 100, 200, 500, 1000, 2000, 4000, 6000, 10000]

fig1, ax1 = plt.subplots()
ax1.hist(shared_votes, bins=bins, log=True, color='orange', linewidth=1.2, edgecolor='black')
ax1.set_xscale('log')
#ax1.set_xticks(bins)
ax1.get_xaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())
plt.show()
