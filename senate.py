# standard library imports
import sys
import itertools
import inspect
import textwrap

# local imports
from senate import const
from senate import util
from senate import docs
from senate import scraper
from senate import dataset
from senate import graph
from senate import cache

# 3rd party imports
import click
import numpy as np
from matplotlib import pyplot as plt
from matplotlib import cm
from matplotlib.colors import ListedColormap

@click.group(context_settings={'help_option_names': ['-h', '--help']})
@click.pass_context
def main(ctx, **kwargs):
  ctx.obj = {**kwargs}

class DatasetGroup:
  @main.group("dataset", help="Manage the dataset.")
  def group(): pass

  @group.command(help="Update the dataset.")
  @click.option('-d', '--dry', is_flag=True, help="Don't actually download anything.")
  @click.option('-p', '--pipeline-filter', 'pipeline_filter', type=int, is_flag=False,
                flag_value=-1, help="Use this flag to only run the pipeline. " +
                "The optional argument specifies which stage of the pipeline " +
                "to run. By default, all stages are run.")
  def update(dry, pipeline_filter):
    dataset.update(dry, pipeline_filter)

  @group.command(help="Remove the dataset.")
  def clean():
    dataset.clean()

  @group.command(help="Show the dataset system status.")
  def status():
    n_votes, n_members, timestamp = dataset.get_system_status()
    print(f"Total Votes: {n_votes}")
    print(f"Total Members: {n_members}")
    print(f"Last Updated: {timestamp}")

class VotesGroup:
  @main.group(help="View vote information.")
  @click.option('-l', '--last', 'last_n', type=int, metavar="N",
                help="Filter only the N most recent votes.")
  @click.option('-r', '--range', '_range', nargs=2, metavar="START STOP",
                default=(None, None),
                help="Filter only votes between the given start and stop.")
  @click.option('-y', '--year', 'year_filter', type=int, metavar="YEAR",
                help="Filter only votes from the given year.")
  @click.option('-n', '--number', 'number_filter', type=int, metavar="NUMBER",
                help="Filter only votes with the given vote number.")
  @click.option('-t', '--threshold', 'threshold_filter', metavar="THRESHOLD",
                type=click.Choice(const.VOTE_THRESHOLDS),
                help="Filter only votes that required the given threshold to pass.")
  @click.option('--passed/--failed', 'result_filter', default=None,
                help="Filter only votes that passed/failed. By default, votes are " +
                     "shown regardless of their outcome.")
  @click.pass_context
  def votes(ctx, last_n, _range, year_filter, number_filter, threshold_filter, result_filter):
    if last_n:
      vote_slice = slice(-last_n, None)
    else:
      vote_slice = dataset.get_term(*_range).to_slice()
    ctx.obj['matching_votes'] = []
    for vote_index, vote in list(enumerate(cache.CacheObj.vote_metadata))[vote_slice]:
      if vote.match(year_filter, number_filter, threshold_filter, result_filter):
        ctx.obj['matching_votes'].append((vote_index, vote))

  @votes.command(short_help="Print information about votes.", help=docs.SHOW_VOTES)
  @click.option('-m', '--mode', metavar='MODE', default=0, show_default=True,
                type=click.IntRange(0,4), help=docs.MODES)
  @click.option('-d', '--description', 'show_description', is_flag=True, default=False,
                help="Show vote descriptions. By default descriptions are hidden.")
  @click.pass_context
  def show(ctx, mode, show_description):
    for vote_index, vote in ctx.obj['matching_votes']:
        dataset.show_vote(vote_index, mode, show_description)

  @votes.command(help="Print a histogram of vote results")
  @click.pass_context
  def result_histogram(ctx):
    results = {}
    for vote_index, vote in ctx.obj['matching_votes']:
      if vote.result not in results:
        results[vote.result] = 0
      results[vote.result] += 1
    for result, count in sorted(results.items(), key=lambda p: p[1], reverse=True):
      print(str(count).rjust(5), result)

  @votes.command(help="Print a histogram of vote scores")
  @click.pass_context
  def score_histogram(ctx):
    scores = {score: 0 for score in range(101)}
    for vote_index, vote in ctx.obj['matching_votes']:
      scores[vote.yeas] += 1

    bins = list(range(101))
    S = np.array(sorted(scores.items(), key=lambda p: p[0]))
    sorted_scores = sorted(scores.items(), key=lambda p: p[1], reverse=True)
    print(util.columnize([f"{s}: {c}" for s, c in sorted_scores], 12))
    heights = S.T[1]
    colors = [str(.75 - .75*greyscale**2) for greyscale in heights/max(heights)]
    plt.bar(bins, heights)
    plt.xticks(bins[::2], bins[::2])
    plt.show()

class MembersGroup:
  @main.group(help="View member information.")
  @click.option('-f', '--first-name', help=\
                "Filter only members whose first name contains TEXT. Not case sensitive.")
  @click.option('-l', '--last-name', help=\
                "Filter only members whose last name contains TEXT. Not case sensitive.")
  @click.option('-s', '--state', type=click.Choice(const.STATE_CODES.keys()), help=\
                "Filter only members representing the given state. Must be a two-letter \
                state code, like WA, NY, CA, etc.", metavar='STATE')
  @click.option('-p', '--party', type=click.Choice('IDR'), help=\
                "Filter only members from the given party.")
  @click.option('-i', '--index', type=int, help="Select a member by their index.")
  @click.pass_context
  def members(ctx, first_name, last_name, state, party, index):
    ctx.obj['matching_members'] = []
    for member_index, member in enumerate(cache.CacheObj.member_metadata):
      if util.filter_match(index, member_index) and \
         member.match(first_name, last_name, state, party):
        ctx.obj['matching_members'].append((member_index, member))

  @members.command(help="Print information about members.")
  @click.pass_context
  def show(ctx):
    vote_metadata = cache.CacheObj.vote_metadata
    member_metadata = cache.CacheObj.member_metadata
    term_trackers = cache.CacheObj.term_trackers
    party_trackers = cache.CacheObj.party_trackers
    member_adjacents = cache.CacheObj.member_adjacents
    for member_index, member in ctx.obj['matching_members']:
      tracker = term_trackers[member_index]
      predecesors, successors = member_adjacents[member_index]
      print(member.long_repr(member_index))
      print(f"Party: {party_trackers[member]}")
      for term_index in range(len(tracker)):
        incumbent = " (incumbent):" if tracker.in_office else ":"
        term = tracker[term_index]
        if term.first == 0:
          predecesor = "(unknown, cache doesn't include pre-1989 data)"
        else:
          predecesor = member_metadata[predecesors[term_index].member_index]
        print(f"Term {term_index+1}{incumbent}")
        print("  First Vote:", vote_metadata[term.first])
        print("  Last Vote:", vote_metadata[term.last])
        print("  Total Votes:", len(term))
        print("  Predecesor:", predecesor)
        if not tracker.in_office:
          print("  Successor:", member_metadata[successors[term_index].member_index])
      print()

  @members.command(help="Show a list of parties that each member has been a part of.")
  @click.option('-s', '--switched', 'switched_only', is_flag=True,
                help="Only show members who have switched parties.")
  @click.pass_context
  def party_history(ctx, switched_only):
    trackers = cache.CacheObj.party_trackers
    for member_index, member in ctx.obj['matching_members']:
      tracker = trackers[member]
      if switched_only and tracker.switched() or not switched_only:
          print(f"{member:<35}", tracker)

@main.command()
@click.option('-g', '--grouped', is_flag=True,
              help="Group members together based on their term length.")
def show_term_histogram(grouped):
  term_trackers = cache.CacheObj.term_trackers
  member_metadata = cache.CacheObj.member_metadata
  length_to_members = {}
  member_to_length = {}
  for member_index, tracker in enumerate(term_trackers):
    length = tracker.max_term()
    util.create_append(length_to_members, length, member_index)
    member_to_length[member_index] = length

  if grouped:
    for length, member_indexes in sorted(length_to_members.items(),
                                         key=lambda e: e[0], reverse=True):
      print(length, ', '.join(member_metadata[i].short_repr() for i in member_indexes))
  else:
    for member_index, length in sorted(member_to_length.items(),
                                      key=lambda e: e[1], reverse=True):
      print(str(length).rjust(5), member_metadata[member_index])

@main.command()
def show_state_seats():
  state_seats = cache.CacheObj.state_seats
  for state_seats in state_seats.values():
    print(state_seats)

@main.command()
def show_year_vote_count():
  totals = cache.CacheObj.total_votes_by_year.items()
  print('\n'.join(f"{year}: {total}" for year, total in totals))

@main.command(help="View agreement percentages for all pairs of members.")
@click.option('-s', '--shared', "shared_only", is_flag=True,
              help="Only show member pairs that have shared votes.")
@click.option('-m', '--member', "members_filter", multiple=True,
              help="Filter only pairs that contain this member.")
def agreements(shared_only, members_filter):
  agreements = cache.CacheObj.agreements
  member_metadata = cache.CacheObj.member_metadata
  n_members = member_metadata.shape[0]
  members_filter = sorted(members_filter)
  L = []
  for a, b in itertools.combinations(range(n_members), 2):
    member_a = member_metadata[a]
    member_b = member_metadata[b]
    last_names = sorted([member_a.name.last, member_b.name.last])
    if len(members_filter) == 1 and (members_filter[0] not in last_names) or\
       (len(members_filter) > 1 and (members_filter != last_names)):
          continue
    total, agrees, p, a2s = agreements[:, a, b]
    if not shared_only or total > 0:
      L.append((member_a,member_b,total,agrees,p, a2s))

  L = sorted(L, key=lambda e: -1 if e[5] is None else e[5], reverse=True)
  for member_a, member_b, total, agrees, p, s1 in L:
    p = None if p is None else f"{p:.3f}"
    s1 = None if p is None else f"{s1:.3f}"
    print(f"{member_a:<36} {member_b:<36} {total:<5} {agrees:<5} {p} {s1}")

@main.command(help="Generate the neighbors graph.")
@click.argument('start', required=False, default=None)
@click.argument('stop', required=False, default=None)
def neighbors(start, stop):
  metrics = {
    'BASIC': 2,
    'ADJUSTED': 3
  }
  metric_type = 'ADJUSTED'
  metric_index = metrics[metric_type]

  member_metadata = cache.CacheObj.member_metadata
  term_filter = dataset.get_term(start, stop)
  similarity = dataset.generate_agreements(term_filter)[metric_index]
  closest = {}
  for (ma, mb) in np.ndindex(similarity.shape):
    s = similarity[ma, mb]
    if s == 0.0: continue
    for m1, m2 in ((ma,mb), (mb,ma)):
      if m1 not in closest: closest[m1] = (0, [])
      if s > closest[m1][0]: closest[m1] = (s, [m2])
      elif s == closest[m1][0]: closest[m1][1].append(m2)

  print("digraph {")
  print('overlap=false;splines=true;rankdir=LR;ratio=1.25;size="30,30";dpi=200;')
  for member, (s, members) in closest.items():
    members_string = ' '.join(f'"{member_metadata[member]}"' for member in members)
    width = 1.0
    if metric_type == 'BASIC':
      label = f"{s*100:.1f}%"
    elif metric_type == 'ADJUSTED':
      label = f"{s:.1f}"

    attr = f'[label="{label}" penwidth="{width}"]'
    print(f'"{member_metadata[member]}" -> {{{members_string}}} {attr}')
  print("}")

class GraphGroup:
  @main.group(help="Generate graphs from the dataset.")
  def graph(): pass

  @graph.command()
  def positions():
    cm = ListedColormap(['white', 'green', 'red', 'blue', 'yellow', 'purple'])
    plt.imshow(cache.CacheObj.positions.T, interpolation='none', cmap=cm,
               aspect='auto')
    plt.colorbar()
    plt.show()

  @graph.command()
  def agreements():
    viridis = cm.get_cmap('viridis', 256)
    newcolors = viridis(np.linspace(0, 1, 256))
    newcolors[:25, :] = np.ones(4)
    newcmp = ListedColormap(newcolors)
    plt.imshow(
      cache.CacheObj.agreements[2] + cache.CacheObj.agreements[2].T,
      cmap=newcmp, interpolation='none'
    )
    plt.colorbar()
    plt.show()

  @graph.command()
  def cohesion():
    vote = cache.CacheObj.vote_metadata[0]
    cohesions = {}

    for vote in cache.CacheObj.vote_metadata:
      if vote.year not in cohesions:
        cohesions[vote.year] = 0
      cohesions[vote.year] += abs(vote.yeas - vote.nays)

    ys = []
    for year, total_cohesion in cohesions.items():
      ys.append(total_cohesion / cache.CacheObj.total_votes_by_year[year])

    years = list(cache.CacheObj.total_votes_by_year.keys())
    plt.xticks(years)
    plt.bar(years, ys)
    plt.show()

if __name__ == '__main__':
  main(obj={})
